function animations(){
  var timeOut2 = setTimeout(function() {
  // Add Scene to ScrollMagic Controller
    var t = new ScrollMagic.Controller,
        e = $(".count");
    e.each(function() {
        var e = $(this),
            i = parseInt(e.attr("data-min"), 10),
            n = parseInt(e.attr("data-max"), 10),
            r = i,
            s = n,
            o = {
                  val: r
                },
            a = !1,
            l = 1.5;
        l = .01 * l,
        l = (n * l).toFixed(2);
        var c = !0;
        c && (l = 1.5);
        var h = function() {
            a = !a,
            a ? (r = i,
            s = n) : (r = n,
            s = i),
            TweenMax.to(o, 1, {
                val: s,
                onUpdate: u,
                ease: Power0.easeNone
            })
        }
          , u = function() {
            e.html(o.val.toFixed())
        }
        ;
        new ScrollMagic.Scene({
            triggerElement: e,
            offset: $('body').offset().top,
            reverse: false
        }).on("enter", h).addTo(t)
    })
    // $('body').css('overflow', 'inherit');
    // $splash.fadeOut();
  }, 2000);
}

/*$( document ).ready(function() {
      setInterval("animations()", 100000);
    });*/

$(document).ready(function(){
  setInterval("location.reload(true)", 120000);
});
