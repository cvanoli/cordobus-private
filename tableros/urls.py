from django.urls import path
from django.conf.urls import url
from django.conf.urls import include
from tableros.views import (index, estado_general, estado_general_mobile,
    internos_por_linea, internos_por_linea_mobile, total_de_boletos_por_dia, estado_general_empresa,
    prueba_graficos_bokeh, InformeNoLogueadosView, OrdenDeServicioView, ReporteEstadoGeneral, ComprometidosVsReales)


urlpatterns = [
    path('estado-general', estado_general, name='tableros.estado-general'),
    path('estado-general-<str:empresa>', estado_general_empresa, name='tableros.estado-general'),
    path('estado-general-mobile', estado_general_mobile, name='tableros.estado-general-mobile'),
    path('internos-por-linea', internos_por_linea, name='tableros.internos-por-linea'),
    path('internos-por-linea-mobile', internos_por_linea_mobile, name='tableros.internos-por-linea-mobile'),
    path('prueba-graficos-bokeh', prueba_graficos_bokeh, name='tableros.prueba-graficos-bokeh'),
    path('informe-no-logueados', InformeNoLogueadosView.as_view(), name='tableros.informe-no-logueados'),
    path('orden-de-servicio/<str:empresa>', OrdenDeServicioView.as_view(), name='tableros.orden-de-servicio'),
    url(r'^planilla-boletos-por-linea.(?P<filetype>csv|xls)$', total_de_boletos_por_dia, name='tableros.total_de_boletos_por_dia'),
    path(r'desvios', include('desvios.urls')),
    url(r'^reporte-general(?:/(?P<desde>\d{2}-\d{2}-\d{4})/(?P<hasta>\d{2}-\d{2}-\d{4}))?/$', ReporteEstadoGeneral, name='reporte.general'),
    path('unidades-comprometidas', ComprometidosVsReales, name='tableros.unidades.comprometidas'),
]