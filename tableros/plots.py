import datetime
from lineas.models import Linea, Recorrido, Empresa
from tracking.models import InternoGPS, RegistroDeFrecuencia
from colectivos.models import (EstacionAnioFrecuencias, DiasGrupoFrecuencias,
                               FranjaHorariaFrecuencias,
                               AgrupadorFranjaHoraria, Frecuencia)
from django.db.models import Count, F
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.palettes import Spectral4
from bokeh.models import ColumnDataSource, LabelSet, HoverTool
from math import pi
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
import numpy as np


def internos_por_linea():

    ahora = datetime.datetime.now()
    dia_str = ahora.strftime('%A')
    hora = ahora.time()

    lineas = Linea.objects.filter(publicado=True).order_by('nombre_publico')
    lineas_nombres = [str(l.nombre_publico) for l in lineas if l.nombre_publico != '']

    colores = {'ERSA': 'rgb(255, 0, 0)', 'Coniferal': 'rgb(231, 119, 0)', 'Autobuses Córdoba': 'rgb(249, 253, 0)', 'TAMSE': 'rgb(30, 163, 1)'}

    # extrae los datos a graficar
    internos = InternoGPS.objects.activos_ahora()

    estacion = EstacionAnioFrecuencias.objects.get(nombre='Normal')

    # se busca la franja horaria de ahora
    if dia_str == 'domingo':
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=3)
        # se busca el agrupador de franja horaria de este momento
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)
    elif dia_str == 'sabado':
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=2)
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)
    else:
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=1)
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)

    #esta forma es con un solo for(mas eficiente)
    x = []
    y = []
    y2 = []
    for linea_nombre in lineas_nombres:
        try:
            f = Frecuencia.objects.get(estacion=estacion, agrupador=franja_horaria.agrupador,
            linea__nombre_publico=linea_nombre)
            y2.append(f.calculo_unidades_activas_esperadas())
        except ObjectDoesNotExist:
            y2.append(None)

    color = []
    label = []
    for linea in lineas_nombres:
        x.append(linea)
        y.append(internos.filter(recorrido_actual__linea__nombre_publico=linea).count())
        color.append(colores[lineas.get(nombre_publico=linea).empresa.nombre_publico])
        label.append(lineas.get(nombre_publico=linea).empresa.nombre_publico)

    source = ColumnDataSource(
            dict(
                x=x,
                y=y,
                color=color,
                label=label
                )
        )

    # construye el grafico
    TOOLTIPS_VBAR = [
        ('Línea', '@x'),
        ('Internos reales', '@y'),
    ]
    TOOLTIPS_CIRCLE = [
        ('Línea', '@x'),
        ('Internos esperados', '@y')
    ]

    plot = figure(x_range=lineas_nombres, toolbar_location=None)
    plot_vbar = plot.vbar(x='x', top='y', width=0.5, bottom=0, color='color', legend='label', source=source)
    plot.add_tools(HoverTool(renderers=[plot_vbar], tooltips=TOOLTIPS_VBAR))

    plot.line(x, y2, line_width=4, legend='Cantidad de internos esperados')
    plot_circle = plot.circle(x, y2, fill_color="white", size=9, legend='Cantidad de internos esperados')
    plot.add_tools(HoverTool(renderers=[plot_circle], tooltips=TOOLTIPS_CIRCLE))

    plot.sizing_mode = 'stretch_both'
    # plot.height = 380
    plot.y_range.start = 0
    plot.legend.orientation = "horizontal"
    plot.xaxis.axis_label = 'Líneas'
    plot.yaxis.axis_label = 'Cantidad de colectivos'
    plot.xaxis.major_label_orientation = pi/4
    # plot.legend.location = "top_left"

    #borro grid horizontal de fondo
    plot.xgrid.grid_line_color = None

    # oculto lineas al clickear leyenda
    plot.legend.click_policy="hide"

    #script js para insertar
    script, div = components(plot)
    return script, div


def plot_frecuencia_por_hora(empresa, responsivo=False, text_x=None, text_y=None):
    """
    Gráfico que muestra el promedio por hora
    """
    ahora = datetime.datetime.now()
    dia_str = ahora.strftime('%A')
    hora = ahora.time()

    x = []
    y = []
    i = 0

    while i < ahora.hour:
        x.append(i)
        regs = RegistroDeFrecuencia.objects.filter(
            momento__timestamp__date=ahora,
            momento__timestamp__time__range=(datetime.time(i),
                                             datetime.time(i+1)),
            linea__empresa__nombre_publico=empresa)

        minuto = 5
        faltantes_por_minutos = []
        while minuto <= 60:
            try:
                regs_fraccionado = regs.filter(momento__timestamp__time__range=(
                    datetime.time(i, minuto-5), datetime.time(i, minuto)))
            except ValueError:
                # obtengo los registros de los últimos 5 minutos
                regs_fraccionado = regs.filter(momento__timestamp__time__range=(
                    datetime.time(i, 55), datetime.time(i, 59, 59)))

            if regs_fraccionado.count() > 0:
                faltantes_por_minutos.append(sum([r.colectivos_faltantes for r in regs_fraccionado]))
            else:
                faltantes_por_minutos.append(0)

            minuto += 5

        np_array = np.array(faltantes_por_minutos)
        # y.append(round(np.mean(np_array))) # promedio
        y.append(round(np.median(np_array))) # mediana

        i += 1

    TOOLTIPS_CIRCLE = [
        ('Hora', '@x'),
        ('Unidades faltantes', '@y')
    ]

    if responsivo:
        plot = figure(toolbar_location=None, title='UNIDADES FALTANTES PROMEDIO POR HORA')
        plot.sizing_mode = 'stretch_both'
    else:
        plot = figure(toolbar_location=None, plot_width=191, plot_height=148,
                        title='UNIDADES FALTANTES PROMEDIO POR HORA')

    plot.line(x, y, line_width=3, line_color='red')

    plot_circle = plot.circle(x, y, fill_color="white", size=5)
    plot.add_tools(HoverTool(renderers=[plot_circle], tooltips=TOOLTIPS_CIRCLE))

    plot.y_range.start = 0

    # borro grid horizontal de fondo
    plot.xgrid.grid_line_color = None

    plot.xaxis.axis_label = text_x
    plot.yaxis.axis_label = text_y
    plot.title.text_font_style = "bold"

    # script js para insertar
    script, div = components(plot)
    return script, div
