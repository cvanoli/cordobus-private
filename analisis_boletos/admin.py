from django.contrib import admin
from .models import (CortesPorHora, CortesPorEmpresa, CortesPorContrato,
                    CortesPorLinea,CortesPorInterno, CortesPorChofer, CortesPorZona)



@admin.register(CortesPorHora)
class CortesPorHoraAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'hora', 'boletos')
    list_filter = ['hora']


@admin.register(CortesPorEmpresa)
class CortesPorEmpresaAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'empresa', 'boletos_empresa')
    list_filter = ['empresa']


@admin.register(CortesPorContrato)
class CortesPorContratoAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'contrato', 'boletos_contrato')
    list_filter = ['contrato']


@admin.register(CortesPorLinea)
class CortesPorLineaAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'linea', 'boletos_linea')
    list_filter = ['linea']


@admin.register(CortesPorInterno)
class CortesPorInternoAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'interno', 'boletos_interno')


@admin.register(CortesPorChofer)
class CortesPorChoferAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'chofer', 'boletos_chofer')


@admin.register(CortesPorZona)
class CortesPorZonaAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'zona', 'boletos_zonas')
    list_filter = ['zona']
