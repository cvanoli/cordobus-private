from django.db import models
from .managers import *
from lineas.models import Empresa, Linea
from boletos.models import Chofer, ContratoUsoBoleto, ZonaBoleto, TarjetaRedBus
from colectivos.models import Interno



class CortesPorHora(models.Model):
    """
    Cortes clasificados por hora en un determinado dia.
    """
    fecha = models.DateField(null=True, blank=True)
    hora = models.CharField(max_length=70, null=True, blank=True, help_text="Rango de una hora. Ej: desde las 17:00 hasta las 17:59")
    boletos = models.PositiveIntegerField(default=0)

    objects = CortesPorHoraManager()

    def __str__(self):
        return 'Fecha: {} - Hora: {} - Boletos: {}'.format(self.fecha, self.hora, self.boletos)

    class Meta:
        permissions = (
            ('ver_tableros_boletos', 'Solo puede ver tableros de boletos'),)


class CortesPorEmpresa(models.Model):
    """
    Cantidad de cortes por empresa en un determinado dia.
    """
    fecha = models.DateField(null=True, blank=True)
    empresa = models.ForeignKey(Empresa, on_delete=models.SET_NULL, null=True, blank=True)
    boletos_empresa = models.PositiveIntegerField(default=0)

    objects = CortesPorEmpresaManager()

    def __str__(self):
        return 'Fecha: {} - Empresa: {} - Boletos: {}'.format(self.fecha, self.empresa, self.boletos_empresa)


class CortesPorContrato(models.Model):
    """
    Cantidad de cortes por contrato en un determinado dia.
    """
    fecha = models.DateField(null=True, blank=True)
    contrato = models.ForeignKey(ContratoUsoBoleto, on_delete=models.SET_NULL, null=True, blank=True)
    boletos_contrato = models.PositiveIntegerField(default=0)

    objects = CortesPorContratoManager()

    def __str__(self):
        return 'Fecha: {} - Contrato: {} - Boletos: {}'.format(self.fecha, self.contrato, self.boletos_contrato)


class CortesPorLinea(models.Model):
    """
    Cantidad de cortes por lineas en un determinado dia.
    """
    fecha = models.DateField(null=True, blank=True)
    linea = models.ForeignKey(Linea, on_delete=models.SET_NULL, null=True, blank=True)
    boletos_linea = models.PositiveIntegerField(default=0)

    objects = CortesPorLineaManager()

    def __str__(self):
        return 'Fecha: {} - Linea: {} - Boletos: {}'.format(self.fecha, self.linea, self.boletos_linea)


class CortesPorInterno(models.Model):
    """
    Cantidad de cortes por internos en un determinado dia.
    """
    fecha = models.DateField(null=True, blank=True)
    interno = models.ForeignKey(Interno, on_delete=models.SET_NULL, null=True, blank=True)
    boletos_interno = models.PositiveIntegerField(default=0)

    objects = CortesPorInternoManager()

    def __str__(self):
        return 'Fecha: {} - Interno: {} - Boletos: {}'.format(self.fecha, self.interno, self.boletos_interno)


class CortesPorChofer(models.Model):
    """
    Cantidad de cortes por choferes en un determinado dia.
    """
    fecha = models.DateField(null=True, blank=True)
    chofer = models.ForeignKey(Chofer, on_delete=models.SET_NULL, null=True, blank=True)
    boletos_chofer = models.PositiveIntegerField(default=0)

    objects = CortesPorChoferManager()

    def __str__(self):
        return 'Fecha: {} - Chofer: {} - Boletos: {}'.format(self.fecha, self.chofer, self.boletos_chofer)


class CortesPorZona(models.Model):
    """
    Cantidad de cortes por zonas en un determinado dia.
    """
    fecha = models.DateField(null=True, blank=True)
    zona = models.ForeignKey(ZonaBoleto, on_delete=models.SET_NULL, null=True, blank=True)
    boletos_zonas = models.PositiveIntegerField(default=0)

    objects = CortesPorZonaManager()

    def __str__(self):
        return 'Fecha: {} - Zona: {} - Boletos: {}'.format(self.fecha, self.zona, self.boletos_zonas)
