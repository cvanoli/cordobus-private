from django.apps import AppConfig


class AnalisisBoletosConfig(AppConfig):
    name = 'analisis_boletos'
