const BASE_URL = "https://cordobus.apps.cordoba.gob.ar/analisis";
const FILE_URL = "/descargable.csv";

const CORTES_EMPRESA_URL = "/cortes-por-empresa";
const CORTES_CONTRATO_URL = "/cortes-por-contrato";
const CORTES_HORA_URL = "/cortes-por-hora";
const CORTES_INTERNOS_URL = "/cortes-por-hora";
const CORTES_CHOFERES_URL = "/choferes-registrados";

$(document).ready(function() {
    registerPageEvents();
});

function registerPageEvents() {
    /*$("#menu_cortesEmpresa > .dropdown-item").click(function() {
        $("#cortesEmpresaMenu").text($(this).text());
    });*/

    $(".dropdown-item").click(function() {
        selectDropdownItem($(this));
    });
}

function selectDropdownItem(curElement) {
    let mainContainer = curElement.parents(".mainCont");
    let dropDownToggle = curElement.parents(".dropdown").children(".dropdown-toggle");
    let csvType = dropDownToggle.attr("id");
    let finalUrl;
    let month = "/" + curElement.attr("data-month");
    let downloadButton = mainContainer.find(".dwnlButton");

    // Actualizamos el texto del selector.
    dropDownToggle.text(curElement.text());

    switch (csvType) {
        case "cortesEmpresaMenu":
            finalUrl = BASE_URL + CORTES_EMPRESA_URL + month + "/2019" + FILE_URL;
            break;

        case "cortesContratoMenu":
            finalUrl = BASE_URL + CORTES_CONTRATO_URL + month + "/2019" + FILE_URL;
            break;

        case "cortesPorHoraMenu":
            finalUrl = BASE_URL + CORTES_HORA_URL + month + "/2019" + FILE_URL;
            break;

        case "internosRegistradosMenu":
            finalUrl = BASE_URL + CORTES_INTERNOS_URL + month + "/2019" + FILE_URL;
            break;

        case "choferesRegistradosMenu":
            finalUrl = BASE_URL + CORTES_CHOFERES_URL + month + "/2019" + FILE_URL;
            break;
    }

    downloadButton.attr("href", finalUrl).removeClass("disabled");
}