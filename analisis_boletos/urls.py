from django.conf.urls import include, url
from django.urls import path
from analisis_boletos.views import *


urlpatterns = [
    url(r'^descargables/$', AnalisisDescargables, name='analisis.descargables'),
    url(r'^cortes-por-empresa(?:/(?P<dias>[0-9]+))?/$', BoletosPorEmpresas, name='cortes.empresa'),
    url(r'^cortes-por-empresa-diario/$', BoletosPorEmpresasDiario, name='cortes.empresa.diario'),
    url(r'^cortes-por-empresa(?:/(?P<mes>[0-9]+))?(?:/(?P<año>[0-9]+))?/descargable.(?P<filetype>csv|xls)$',
        BoletosPorEmpresasDescargable, name='cortes.empresa.descargable'),
    url(r'^cortes-por-contrato(?:/(?P<dias>[0-9]+))?/$', BoletosPorContrato, name='cortes.contrato'),
    url(r'^cortes-por-contrato(?:/(?P<mes>[0-9]+))?(?:/(?P<año>[0-9]+))?/descargable.(?P<filetype>csv|xls)$',
        BoletosPorContratoDescargable, name='cortes.empresa.contrato.descargable'),
    url(r'^cortes-por-linea(?:/(?P<dias>[0-9]+))?/$', BoletosPorLinea, name='cortes.lineas'),
    url(r'^cortes-por-linea(?:/(?P<mes>[0-9]+))?(?:/(?P<año>[0-9]+))?/descargable.(?P<filetype>csv|xls)$',
        BoletosPorLineasDescargable, name='cortes.empresa.lineas.descargable'),
    url(r'^cortes-por-linea-diario/$', BoletosPorLineaConDescripcion, name='cortes.lineas.descripcion'),
    url(r'^cortes-por-hora(?:/(?P<dias>[0-9]+))?/$', BoletosPorHora, name='cortes.hora'),
    url(r'^cortes-por-hora-diario/$', BoletosPorHoraDiario, name='cortes.hora.diario'),
    url(r'^cortes-por-hora(?:/(?P<mes>[0-9]+))?(?:/(?P<año>[0-9]+))?/descargable.(?P<filetype>csv|xls)$',
        BoletosPorHoraDescargable, name='cortes.empresa.hora.descargable'),
    #url(r'^cortes-por-contrato-agrupado(?:/(?P<dias>[0-9]+))?/$', BoletosPorContratoAgrupado, name='cortes.contrato.agrupado'),
    url(r'^cortes-con-subsidio(?:/(?P<dias>[0-9]+))?/$', BoletosConSubsidio, name='cortes.sin.subsidio'),
    url(r'^cortes-con-subsidio-diario/$', BoletosConSubsidioDiario, name='cortes.con.subsidio.diario'),
    url(r'^reporte-transporte(?:/(?P<desde>\d{2}-\d{2}-\d{4})/(?P<hasta>\d{2}-\d{2}-\d{4}))?/$', InformeSemanal, name='reporte.semanal'),
    url(r'^cortes-por-zona(?:/(?P<dias>[0-9]+))?/$', BoletosPorZona, name='cortes.por.zona'),
    url(r'^internos-registrados(?:/(?P<dias>[0-9]+))?/$', InternosRegistrados, name='internos.registrados'),
    url(r'^internos-registrados(?:/(?P<mes>[0-9]+))?(?:/(?P<año>[0-9]+))?/descargable.(?P<filetype>csv|xls)$',
        InternosRegistradosDescargable, name='cortes.empresa.internos.descargable'),
    url(r'^choferes-registrados(?:/(?P<dias>[0-9]+))?/$', ChoferesRegistrados, name='choferes.registrados'),
    url(r'^choferes-registrados(?:/(?P<mes>[0-9]+))?(?:/(?P<año>[0-9]+))?/descargable.(?P<filetype>csv|xls)$',
        CortesPorChoferDescargable, name='cortes.empresa.choferes.descargable'),
    url(r'^choferes-registrados-diario/$', ChoferesRegistradosDiario, name='choferes.registrados.diario'),
    url(r'^cortes-por-servicio-especial(?:/(?P<dias>[0-9]+))?/$', BoletosPorEmpresaServicioEspecial, name='cortes.servicio.especial'),
]
