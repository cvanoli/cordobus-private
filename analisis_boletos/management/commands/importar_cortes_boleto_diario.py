#!/usr/bin/python
"""
Importar datos de cortes de boleto desde webservice FTP del proveedor WorldLine
"""
from django.core.management import call_command
from django.core.management.base import BaseCommand
from analisis_boletos import settings as settings_tp
from django.db import transaction
import sys
import ftplib
import os


class Command(BaseCommand):
    help = """Comando para importar datos de cortes de boleto del transporte desde servidor del proveedor WoldLine """

    """
    def add_arguments(self, parser):
        parser.add_argument('--fechaDesde', nargs='?', type=str, help='Fecha de registro en el civil (Y-m-d) para traer los nacimientos Desde')
        parser.add_argument('--fechaHasta', nargs='?', type=str, help='Fecha de registro en el civil (Y-m-d) para traer los nacimientos Hasta')
    """

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación de datos de cortes de boleto.'))

        try:
            #conexion
            host = settings_tp.IP_FTP
            user = settings_tp.USER_FTP
            passwd = settings_tp.PASS_FTP
            ftps = ftplib.FTP_TLS()
            ftps.connect(host, settings_tp.PUERTO_FTP)
            ftps.login(user, passwd)
            self.stdout.write(self.style.SUCCESS('Conectando a {}'.format(host)))
        except Exception as e:
            self.stdout.write(self.style.ERROR('ERROR trayendo datos: {}'.format(e)))
            sys.exit(1)

        #cambio a conexion segura
        ftps.prot_p()
        archivos_nuevos = 0
        ruta_absoluta = settings_tp.PATH_SAVE
        try:
            # nlst retorna los nombres de archivos del servidor
            files = ftps.nlst()

            for f in files:
                # indica si hay que procesar el archivo
                procesar = True
                # si el archivo ya existe, lo omito
                if not(os.path.isfile(ruta_absoluta + f)):

                    with open(ruta_absoluta + f, 'wb') as fp:
                        self.stdout.write(self.style.SUCCESS('Descargando {}'.format(f)))
                        res = ftps.retrbinary('RETR '+f, fp.write)

                        if not res.startswith('226 Transfer complete'):
                            self.stdout.write(self.style.ERROR('Falló la descarga de: {}'.format(f)))
                            if os.path.isfile(ruta_absoluta + f):
                                os.remove(ruta_absoluta + f)
                        else:
                            self.stdout.write(self.style.SUCCESS('Importación exitosa de {} \n'.format(f)))
                            archivos_nuevos += 1

                else:
                    self.stdout.write(self.style.ERROR('Omitiendo {}, el archivo ya existe en {}'.format(f, ruta_absoluta)))
                    procesar = False

                if procesar:
                    # se ejecuta el comando de proceso del archivo descargado
                    # hacerlo dentro de la instruccion open causa un error
                    call_command('cortes_por_dia', '--path='+ruta_absoluta+f)

        except ftplib.all_errors as e:
            self.stdout.write(self.style.ERROR('FTP ERROR: {}'.format(e)))
            if os.path.isfile(ruta_absoluta + f):
                os.remove(ruta_absoluta + f)

        try:
            ftps.quit()
        except ftplib.all_errors as e:
            self.stdout.write(self.style.ERROR('FTP ERROR AL CORTAR LA CONEXIÓN: {}'.format(e)))
            ftps.close()
        self.stdout.write(self.style.SUCCESS('FIN. {} archivo/s nuevo/s'.format(archivos_nuevos)))
