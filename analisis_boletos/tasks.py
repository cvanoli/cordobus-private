#!/usr/bin/python
"""
Importar datos de cortes de boleto desde webservice FTP del proveedor WorldLine
"""
from django.core.management import call_command
from analisis_boletos import settings as settings_tp
from django.db import transaction
import sys
import ftplib
import os
from celery import Celery
from cordobus.celeryconf import app

@app.task
@transaction.atomic
def importar_cortes_boleto():
    call_command('importar_cortes_boleto_diario')
    return
