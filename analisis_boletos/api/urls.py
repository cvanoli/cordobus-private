from django.conf.urls import url, include
from rest_framework import routers
from .views import *


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'cortes-por-hora', CortesPorHoraViewSet, base_name='cortes-por-hora.api.lista')
router.register(r'cortes-por-empresa', CortesPorEmpresaViewSet, base_name='cortes-por-empresa.api.lista')
router.register(r'cortes-por-contrato', CortesPorContratoViewSet, base_name='cortes-por-contrato.api.lista')
router.register(r'cortes-por-interno', CortesPorInternoViewSet, base_name='cortes-por-interno.api.lista')
router.register(r'cortes-por-chofer', CortesPorChoferViewSet, base_name='cortes-por-chofer.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
