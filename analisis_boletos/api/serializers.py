from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework import serializers
from rest_framework_cache.registry import cache_registry
from analisis_boletos.models import (CortesPorHora, CortesPorEmpresa, CortesPorContrato,
                                    CortesPorLinea, CortesPorInterno, CortesPorChofer)



class CortesPorHoraSerializer(CachedSerializerMixin):

    class Meta:
        model = CortesPorHora
        fields = ['id', 'fecha', 'hora', 'boletos']


class CortesPorEmpresaSerializer(CachedSerializerMixin):
    empresa = serializers.CharField(source='empresa.nombre_publico')
    id_empresa = serializers.IntegerField(source='empresa.id')

    class Meta:
        model = CortesPorEmpresa
        fields = ['id', 'fecha', 'id_empresa', 'empresa', 'boletos_empresa']


class CortesPorContratoSerializer(CachedSerializerMixin):
    id_contrato = serializers.IntegerField(source='contrato.id')
    contrato = serializers.CharField(source='contrato.tipo_contrato')

    class Meta:
        model = CortesPorContrato
        fields = ['id', 'fecha', 'id_contrato', 'contrato', 'boletos_contrato']


class CortesPorLineaSerializer(CachedSerializerMixin):

    class Meta:
        model = CortesPorLinea
        fields = ['id', 'fecha', 'linea', 'boletos_linea']


class CortesPorInternoSerializer(CachedSerializerMixin):

    class Meta:
        model = CortesPorInterno
        fields = ['id', 'fecha', 'interno', 'boletos_interno']


class CortesPorChoferSerializer(CachedSerializerMixin):

    class Meta:
        model = CortesPorChofer
        fields = ['id', 'fecha', 'chofer', 'boletos_chofer']



# Registro los serializadores en la cache de DRF
cache_registry.register(CortesPorHoraSerializer)
cache_registry.register(CortesPorEmpresaSerializer)
cache_registry.register(CortesPorContratoSerializer)
cache_registry.register(CortesPorLineaSerializer)
cache_registry.register(CortesPorInternoSerializer)
cache_registry.register(CortesPorChoferSerializer)
