from django.db import models
import datetime
import pytz
from django.utils import timezone
from django.db.models import Q
from django.conf import settings
from zonas.models import Zonas
from django.contrib.gis.geos import MultiPolygon



class TrackingManager(models.Manager):
    """
    Datos específicos del tracking del transporte público
    """
    def viaje_filtrado(self, viaje, dias=None):
        """
        Retorna el tracking de un viaje (interno_en_recorrido), aplicando algunas limpiezas
        de momento solo retorna el ultimo dia de tracking, ya que hay viajes registrados durante
        muchos dias y realmente corresponden a muchos viajes
        :param dias: especifica la cantidad maxima de dias que se quieren mostrar como tracking de un recorrido,
        se recomienda dejar en 1
        :return: QS de tracking prefiltrado
        """
        if dias:
            dias = datetime.timedelta(days=dias)
            timestamp_inicio = viaje.terminado - dias
            timestamp_fin = viaje.terminado
            qs = self.filter(viaje=viaje,
                             timestamp__gte=timestamp_inicio, timestamp__lte=timestamp_fin)
        else:
            qs = self.filter(viaje=viaje)
        return qs


class LineaManager(models.Manager):
    """
    Datos específicos de líneas del transporte público
    """
    pass
    

class InternoEnRecorridoManager(models.Manager):

    # Rotacion de internos por dias
    def recorridos_activos(self, dias=3):
        """
        Recorridos activos en los ultimos 'dias'.
        Nota: En esta query estan incluidos los recorridos vacios asociados a un interno,
        por ejemplo: <InternoEnRecorrido: 1836 en >. Es por esto que tenemos muchos mas resultados
        que en el manager de RecorridoManager -> recorridos_actuales.
        """
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=dias)
        qs = self.filter(ultimo_vez__gte=desde)
        return qs

    # internos activos por segundos
    def activos(self, segundos=600):
        # Mejorar excluyendo los de punta de linea, etc
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(seconds=settings.SEGUNDOS_SIN_TRACKING_PARA_CONSIDERAR_ACTIVO)
        qs = self.filter(ultimo_vez__gte=desde)
        return qs

class RecorridoManager(models.Manager):
    """
    Datos específicos de recorridos del transporte público
    """

    def recorridos_actuales(self, dias=2):
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=dias)
        r = self.distinct().filter(internos__ultimo_vez__gte=desde)
        return r

    def matcheados(self):
        # Recorridos matcheados y publicados (matcheados a mano)
        # OJO: No significa que esten bien matcheados!
        recorridos = self.filter(publicado=True, id_externo__gt=0)
        return recorridos

    def no_matcheados(self):
        # Recorridos no matcheados y no publicados.
        recorridos = self.filter(publicado=False, id_externo=0)
        return recorridos

    def matcheados_no_publicados(self):
        # Recorridos matcheados pero no publicados.
        # Puede ser porque aparecio un nuevo recorrido y se dejo de usar el otro
        recorridos = self.filter(publicado=False, id_externo__gt=0)
        return recorridos

    def ultimos_registrados(self, dias=15):
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=dias)
        # Por default los nuevos recorridos que nos llegan ingresan publicados pero con id_externo=0
        qs = self.filter(publicado=True, id_externo=0, creado__gte=desde)
        #return qs.distinct() Si me llegan a mandar duplicados los quiero ver
        return qs

class InternoGPSManager(models.Manager):
    """
    Datos específicos de colectivos del transporte público
    """
    def activos_ahora(self, seconds=600):
        """
        colectivos activos ahora mismo. 
        Predeterminado los que reportaron en los últimos 5 minutos.
        Opcionalmente grabar el status (de recorridos) de actividad de este momento 
        (para tener históricos más livianos)
        """
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(seconds=settings.SEGUNDOS_SIN_TRACKING_PARA_CONSIDERAR_ACTIVO)

        lista_poligonos = [zona.poligono for zona in Zonas.objects.filter(es_zona_de_fuera_de_servicio=True) if zona.poligono is not None]
        multipoligono = MultiPolygon(lista_poligonos)
        qs = self.exclude(ultima_posicion_conocida__within=multipoligono).filter(Q(ultima_posicion_time__gt=desde))
        return qs.distinct()

    def en_servicio(self, dias=7):
        """
        colectivos que podríamos decir que están en servicio y son parte de la plantilla
        """
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=dias)
        qs = self.filter(ultima_posicion_time__gt=desde)
        return qs.distinct()

    def registrados(self):
        """
        colectivos que registran el recorrido que están haciendo
        """
        qs = self.activos_ahora().exclude(Q(recorrido_actual__isnull=True), Q(recorrido_actual__descripcion_corta=''))
        return qs.distinct()
    
    def no_registrados(self):
        """
        colectivos que registran el recorrido que están haciendo
        """
        qs = self.activos_ahora().filter(Q(recorrido_actual__isnull=True) | Q(recorrido_actual__descripcion_corta=''))
        return qs.distinct()

    def en_zona_de_fuera_de_servicio(self):
        """
        colectivos que están en zonas marcadas como fuera de servicio (puntas de linea y similar)
        """
        lista_poligonos = [zona.poligono for zona in Zonas.objects.filter(es_zona_de_fuera_de_servicio=True) if zona.poligono is not None]
        multipoligono = MultiPolygon(lista_poligonos)
        qs = self.filter(ultima_posicion_conocida__within=multipoligono)
        return qs.distinct()

    def en_zona_de_media_punta(self):
        """
        colectivos que están en zonas marcadas media punta
        """
        lista_poligonos = [zona.poligono for zona in Zonas.objects.filter(es_media_punta=True) if zona.poligono is not None]
        multipoligono = MultiPolygon(lista_poligonos)
        qs = self.filter(ultima_posicion_conocida__within=multipoligono)
        return qs.distinct()
    
    def adaptados_activos(self):
        """ unidades adaptadas y de piso bajo activas QUE ESTAN ACTIVAS """
        qs = self.activos_ahora().filter(Q(piso_bajo=True) | Q(adaptado=True))
        return qs.distinct()
    
    def adaptados_en_servicio(self):
        """ unidades adaptadas y de piso bajo activas QUE ESTAN ACTIVAS """
        qs = self.en_servicio().filter(Q(piso_bajo=True) | Q(adaptado=True))
        return qs.distinct()
