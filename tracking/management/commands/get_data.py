import pytz
from django.contrib.gis.geos import Point
from django.core.management.base import BaseCommand
from scapy.all import *
from scapy.layers.inet import UDP
from datetime import timedelta, datetime
from tracking.models import (Tracking, InternoGPS, InternoEnRecorrido,
                                Empresa, Viaje)
from lineas.models import Linea, Recorrido
from tracking.queues import MyQueues
from django.conf import settings
from django.utils.text import slugify


class Command(BaseCommand):
    help = "Obtengo informacion de colectivos"
    queue_velocidades = MyQueues(largo=5)
    # aún estando quietos mandan velocidades !!!
    # guardo las posiciones para saber si la variacion es nula es porque están quietos
    queue_latitudes = MyQueues(largo=5)
    queue_longitudes = MyQueues(largo=5)

    # almacenar los datos del ultimo reporte de cada coche
    # para no guardar datos de más innecesarios
    # ultimos_reportes_por_interno = {}
    # diferencia_minima_segundos_entre_reportes_para_grabarlos = timedelta(seconds=30)

    def packet_received(self, packet):

        """
        DESCRIPCION
        $Linea|Interno|Ramal|FechaUTC|Latitud|Longitud|Velocidad|CodigoEquipo|CodigoEmpresa|CodigoVehiculo$ 
        EJEMPLO
        '$70|2959 (R)|MIVE|05/09/2018 17:31:28|-31,424698|-64,16877|35|4016003|154|496$'
        '$27|3054|SNCE|05/09/2018 17:31:35|-31,373764|-64,207161|18|4018798|154|271$'
        '$18|030|18 I|05/09/2018 17:31:25|-31,388842|-64,209587|18|4015900|175|26$'
        '$32|2664|RISI|05/09/2018 17:31:36|-31,352745|-64,176453|6|4015111|154|523$'
        '$62|139|62 V|05/09/2018 17:31:33|-31,423065|-64,233994|0|4016033|175|196$'
        '$41|1896 (PB)|RL41|05/09/2018 17:31:34|-31,374331|-64,176003|25|4018885|361|1181$'
        '$|2702||05/09/2018 17:31:26|-31,41646|-64,186493|1|4014326|154|535$'
        '$54|3471 (R)|IL54|05/09/2018 17:31:22|-31,411116|-64,18486|0|4018817|154|2269$'
        '$23|3198|PLSP|05/09/2018 17:31:31|-31,410606|-64,193535|38|4018155|154|335$'
        '$50|3467 (R)|RL50_V|05/09/2018 17:31:34|-31,379124|-64,153488|44|4018760|154|1871$'
        '$27|3043|CESN|05/09/2018 17:31:31|-31,418808|-64,178391|17|4015090|154|235$'
        Los sufijos A o (A) significan "Adaptado"
        Los sufijos P o (PB) significan "Piso bajo"
        """

        try:
            packet = packet[UDP].payload
            packet_str = str(bytes(packet))
            partes = packet_str.split("|")

            self.stdout.write(self.style.WARNING('{} {}'.format(len(partes), packet_str)))

            linea, interno, recorrido, timestamp, latitud, longitud, velocidad, id_equipo_gps, id_empresa, codigo_vehiculo = partes

            velocidad_int = int(velocidad.strip())
        except Exception as e:
            self.stdout.write(self.style.ERROR('Error al importar: {}'))
            # a veces no existe packet_str self.stdout.write(self.style.ERROR('Paquete: {}'.format(packet_str)))
            #FIXME mandar a sentry o algo
            return

        # Fixes
        linea = linea.replace("b'$", "").strip().upper()
        recorrido = slugify(recorrido.strip())
        latitud = float(latitud.replace(",", "."))
        longitud = float(longitud.replace(",", "."))
        timestamp = datetime.strptime(timestamp, "%d/%m/%Y %H:%M:%S") - timedelta(hours=3)
        timestamp = pytz.timezone("America/Argentina/Cordoba").localize(timestamp, is_dst=None)
        id_equipo_gps = int(id_equipo_gps.strip())
        id_empresa = int(id_empresa.strip())
        codigo_vehiculo = codigo_vehiculo.replace("$'", "").strip().upper()

        empresa, created = Empresa.objects.get_or_create(id_externo=id_empresa)

        interno = interno.strip().upper()

        articulado = False
        if interno.find(' ART') > 0:
            articulado = True
            interno.replace(' ART', '')

        adaptado = False
        adaptados = [' A', ' (A)', '(A)']
        for a in adaptados:
            if interno.find(a) > 0:
                interno.replace(a, '')
                adaptado = True

        piso_bajo = False
        pisos_bajos = [' PB', ' (PB)', '(PB)']
        for a in pisos_bajos:
            if interno.find(a) > 0:
                interno.replace(a, '')
                piso_bajo = True

        if interno.find(" ") > 0:
            #FIXME necesito saber que es, grabarlo para saber despues
            p = interno.split()
            interno = p[0]
            desconocidos = ' '.join(p[1:])

        # Instances
        linea_instance, created = Linea.objects.get_or_create(nombre_publico=linea)
        linea_instance.empresa = empresa
        linea_instance.save()

        interno_instance, created = InternoGPS.objects.get_or_create(codigo_vehiculo=codigo_vehiculo, name=interno)
        interno_instance.piso_bajo = piso_bajo
        interno_instance.articulado = articulado
        interno_instance.adaptado = adaptado
        interno_instance.empresa = empresa

        print("Recorrido: ", recorrido)
        print("Linea: ", linea_instance.nombre_publico)
        recorrido_instance, created = Recorrido.objects.get_or_create(descripcion_corta=recorrido, linea=linea_instance)

        interno_en_recorrido_instance, created = InternoEnRecorrido.objects.get_or_create(interno=interno_instance, recorrido=recorrido_instance)
        interno_en_recorrido_instance.ultimo_vez = timestamp
        interno_en_recorrido_instance.save()
        # Create point
        point = Point(longitud, latitud)

        # almacenar los últimos 10 datos de velocidad y posicion

        # ver si esta vivo viendo sus últimas velocidades
        self.queue_velocidades.append(sid=interno_instance.id, elem=velocidad_int)
        varc = self.queue_velocidades.variacion(sid=interno_instance.id) or -1.0
        interno_instance.ultimas_velocidades_promedio = varc
        lista_velocidades = self.queue_velocidades.listas[interno_instance.id]

        # guardar las posiciones
        self.queue_latitudes.append(sid=interno_instance.id, elem=latitud)
        self.queue_longitudes.append(sid=interno_instance.id, elem=longitud)

        var_latitud = self.queue_latitudes.variacion(sid=interno_instance.id) or 0.0
        interno_instance.ultimas_variaciones_latitud = var_latitud
        var_longitud = self.queue_longitudes.variacion(sid=interno_instance.id) or 0.0
        interno_instance.ultimas_variaciones_longitud = var_longitud

        res = "{}-{}-{} T{} ({},{}) {}km/h {}".format(linea, interno, recorrido, timestamp, latitud, longitud, velocidad, lista_velocidades)
        self.stdout.write(self.style.SUCCESS(res))
        # self.ultimos_reportes_por_interno[interno] = pytz.timezone("America/Argentina/Cordoba").localize(datetime.now())  # no confiar tanto en "timestamp"

        interno_instance.ultima_posicion_time = timestamp
        interno_instance.ultima_posicion_conocida = point

        # si cambia el recorrido entonces empieza un nuevo viaje
        if interno_instance.recorrido_actual != recorrido_instance or interno_instance.ultimo_viaje is None:
            interno_instance.ultimo_viaje = Viaje.objects.create(interno=interno_instance,
                                                                 recorrido=recorrido_instance,
                                                                 iniciado=timestamp,
                                                                 terminado=timestamp)
        else:
            interno_instance.ultimo_viaje.terminado = timestamp
            interno_instance.ultimo_viaje.save()

        interno_instance.recorrido_actual = recorrido_instance

        if recorrido != '':  # sirve si mañana sale sin loguearse
            interno_instance.ultimo_recorrido_ok = recorrido_instance
        interno_instance.save()

        # Save tracking
        tracking = Tracking(
            interno_en_recorrido=interno_en_recorrido_instance,
            velocidad=velocidad_int,
            location=point,
            timestamp=timestamp,
            viaje=interno_instance.ultimo_viaje
        )
        tracking.save()

    def handle(self, *args, **options):
        flt = 'port {}'.format(settings.LISTEN_UDP_PORT)
        sniff(filter=flt, prn=self.packet_received)
