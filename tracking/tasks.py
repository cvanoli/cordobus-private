#!/usr/bin/python
from django.db import transaction
import sys, os, datetime, locale
from celery import Celery
from cordobus.celeryconf import app
from colectivos.models import (EstacionAnioFrecuencias, DiasGrupoFrecuencias,
    FranjaHorariaFrecuencias, AgrupadorFranjaHoraria, Frecuencia)
from tracking.models import (InternoGPS, MomentoRegistroFrecuencia,
    RegistroDeFrecuencia, RegistroDeDeslogueadoFrecuencia)
from lineas.models import Linea, Empresa
from colectivos.models import Interno
from django.shortcuts import get_object_or_404
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist


def calcular_frecuencias(orden=False):
    """
    Retorna una 3-upla:
    ranking_empresa: dict de la forma {'empresa': cantidad_internos_faltantes}
    porcentaje_lineas: dict de la forma
        {'empresa': [('nombre_linea',
                    (internos_reales, internos_esperados, porcentaje_flota_circulando, cantidad_internos_faltantes, [lista con codigo de internos reales]))]}
    deslogueados: dict de la forma {'empresa': [lista de codigos de deslogueados]}
    """
    locale.setlocale(locale.LC_ALL, 'es_AR')
    ahora = datetime.datetime.now()
    dia_str = ahora.strftime('%A')
    hora = ahora.time()

    # FIXME: esto hay que corregir de alguna manera, en un futuro se agregarán estaciones
    estacion = EstacionAnioFrecuencias.objects.get(nombre='Normal')

    # se busca la franja horaria de ahora
    if dia_str == 'domingo':
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=3)
        # se busca el agrupador de franja horaria de este momento
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)
    elif dia_str == 'sabado':
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=2)
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)
    else:
        grupo_dia = DiasGrupoFrecuencias.objects.get(id=1)
        franja_horaria = get_object_or_404(FranjaHorariaFrecuencias, hora_desde__lte=hora, hora_hasta__gte=hora,
                            agrupador__dias=grupo_dia)

    lineas = Linea.objects.exclude(nombre_publico='')
    lineas_nombres = [str(l.nombre_publico) for l in lineas if l.nombre_publico != '']
    internos = InternoGPS.objects.activos_ahora()
    no_logueados = internos.filter(Q(recorrido_actual__isnull=True) | Q(recorrido_actual__descripcion_corta=''))
    empresas = Empresa.objects.all()
    lista_no_logueados = no_logueados.values_list('name', flat=True)
    internos_registrados = Interno.objects.filter(numero__in=lista_no_logueados)

    ranking = {}
    deslogueados = {}
    for e in empresas:
        lineas_por_empresa = lineas.filter(empresa__nombre_publico=e.nombre_publico)
        frecuencia = Frecuencia.objects.filter(estacion=estacion,
                                        agrupador=franja_horaria.agrupador
                                        )

        ranking[e.nombre_publico] = {}
        for linea_nombre in lineas_por_empresa:
            try:
                int_reales = internos.filter(recorrido_actual__linea__nombre_publico=linea_nombre.nombre_publico)
                f = frecuencia.get(linea__nombre_publico=linea_nombre.nombre_publico)
                ranking[e.nombre_publico][linea_nombre.nombre_publico] = (
                    int_reales.count(),
                    f.calculo_unidades_activas_esperadas(),
                    [i.name for i in int_reales]
                    )
            except ObjectDoesNotExist:
                pass

        deslogueados[e.nombre_publico] = [d.numero for d in internos_registrados.filter(empresa__nombre_publico=e.nombre_publico)]

    ranking_empresa = {}
    porcentaje_lineas = {}
    for k,v in ranking.items():
        # el valor será un contador
        ranking_empresa[k] = 0

        # el valor será el porcentaje de unidades activas
        porcentaje_lineas[k] = []

        # itero sobre el dict de frecuencia
        for k1,v1 in v.items():
            # sólo sumo cuando la frecuencia es menor de la esperada
            ranking_empresa[k] += v1[1]-v1[0] if v1[1] > v1[0] else 0

            # si el porcentaje es 0, no lo agrego al resultado
            porc = (v1[0]/v1[1] if v1[0] != 0 else 0)*100
            if porc != 0:
                porcentaje_lineas[k].append((k1, (v1[0], v1[1], porc, v1[1]-v1[0], v1[2])))

    if orden:
        # ordeno este dic por porcentaje, y sólo se pasa los 3 primeros
        porcentaje_lineas = {k:sorted(v, key=lambda porcentaje: porcentaje[1][3], reverse=True)[:3] for k,v in porcentaje_lineas.items()}

    return (ranking_empresa, porcentaje_lineas, deslogueados)


@app.task
@transaction.atomic
def grabar_frecuencias():
    ranking_empresa, porcentaje_lineas, deslogueados = calcular_frecuencias()
    momento = MomentoRegistroFrecuencia.objects.create()

    for empresa, lista in porcentaje_lineas.items():
        for linea, tupla in lista:
            r = RegistroDeFrecuencia(
                momento=momento,
                linea=Linea.objects.get(nombre_publico=linea),
                unidades_esperadas=tupla[1]
                )
            # necesario para poder guardar el manytomany
            r.save()
            r.unidades_circulando.set(Interno.objects.filter(numero__in=tupla[4]).filter(empresa__nombre_publico=empresa))
            r.save()

    for empresa, lista_no_logueados in deslogueados.items():
        r = RegistroDeDeslogueadoFrecuencia(
            momento=momento,
            empresa=Empresa.objects.get(nombre_publico=empresa)
            )
        r.save()
        r.deslogueados.set(Interno.objects.filter(numero__in=lista_no_logueados))
        r.save()

    return
