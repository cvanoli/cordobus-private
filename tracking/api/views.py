import datetime
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated, DjangoModelPermissionsOrAnonReadOnly
from rest_framework.viewsets import ReadOnlyModelViewSet, ViewSet
from .serializers import InternosActivosSerializer, RegistroDeFrecuenciaSerializer
from tracking.models import InternoGPS, RegistroDeFrecuencia
# from common.helpers.desvios import desvios_modelos, validar_viaje_modelo
from core.pagination import BondiMapaPagination
# from rest_framework.response import Response


class InternosActivosViewSet(viewsets.ModelViewSet):
    """
    Colectivos activos en un momento determinado.
    Puede filtrarse por
    segundos: Retorna los colectivos activos de los últimos "segundos". Por defecto
            se retornan los colectivos activos de los últimos 10 minutos(600 segundos).
    linea: Retorna los colectivos activos de la linea seleccionada. Este parámetro toma
    el nombre público de la línea, por ejemplo 40, 51, 73, etc..
    id_recorrido: Retorna los colectivos activos del recorrido seleccionado. Este parámetro
    toma el id del recorrido. Para consultar recorridos ir a https://cordobus.apps.cordoba.gob.ar/lineas/api/recorrido/
    """
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = InternosActivosSerializer
    pagination_class = BondiMapaPagination

    def get_queryset(self):

        segundos = self.request.query_params.get('segundos', None)
        if segundos:
            queryset = InternoGPS.objects.activos_ahora(seconds=int(segundos))
        else:
            queryset = InternoGPS.objects.activos_ahora(seconds=600)

        linea = self.request.query_params.get('linea', None)
        if linea:
            queryset = queryset.filter(recorrido_actual__linea__nombre_publico=linea)

        id_recorrido = self.request.query_params.get('id_recorrido', None)
        if id_recorrido:
            recorridos = id_recorrido.split(',')
            queryset = queryset.filter(recorrido_actual__id__in=recorridos)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RegistroDeFrecuenciaViewSet(ReadOnlyModelViewSet):

    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = RegistroDeFrecuenciaSerializer
    pagination_class = None

    def get_queryset(self):
        id_linea = int(self.kwargs['id_linea'])
        try:
            #FIXME Maycol, esta linea gasta plata al pedo
            registro = RegistroDeFrecuencia.objects.filter(linea_id=id_linea).order_by('-id')[:1]
        except RegistroDeFrecuencia.DoesNotExist:
            return RegistroDeFrecuencia.objects.none()
        
        return registro
