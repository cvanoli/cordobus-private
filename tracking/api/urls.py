from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .views import InternosActivosViewSet, RegistroDeFrecuenciaViewSet

router = DefaultRouter()
router.register(r'internos-activos-ahora', InternosActivosViewSet, base_name='internos-activos-geo')
router.register(r'registro-de-frecuencias/(?P<id_linea>[0-9]+)', RegistroDeFrecuenciaViewSet, base_name='registro-de-frecuencias')

urlpatterns = [
    url(r'^', include(router.urls)),
]
