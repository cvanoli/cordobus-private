from rest_framework import permissions
import requests
from django.core.cache import cache


class IsAuthenticatedInQhapax(permissions.BasePermission):
    """ Que el usuario sea válido en Qhapax """

    def has_permission(self, request, view):
        http_autorization = request.META.get('HTTP_AUTHORIZATION', None)
        # ejemplo: .query_params: <QueryDict: {'param-algo': ['90AA']}>

        cache_key = 'auth_{}'.format(http_autorization)
        if cache.get(cache_key):
            return cache.get(cache_key)

        headers = {'Authorization': http_autorization}
        url_with_token = 'https://gobiernoabierto.cordoba.gob.ar/api/v2/redbus-data/saldo-tarjeta-redbus/'
        req = requests.post(url_with_token, headers=headers)
        res = req.json()
        # token no es: '{"detail":"Token inv\xc3\xa1lido."}'
        # token OK: b'{"error":"No se incluye el c\xc3\xb3digo de la tarjeta"}'

        ret = False
        
        if 'error' in res.keys() and res['error'] == "No se incluye el código de la tarjeta":
            # cache de 5h
            cache.set(cache_key, ret, 60 * 60 * 5)
            ret = True

        if 'detail' in res.keys() and res['detail'] == "Token inválido":
            ret = False

        
        return ret
        

    def has_object_permission(self, request, view, obj):

        cache_key = 'auth_{}'.format(http_autorization)
        if cache.get(cache_key):
            return cache.get(cache_key)

        http_autorization = request.META.get('HTTP_AUTHORIZATION', None)
        # ejemplo: .query_params: <QueryDict: {'param-algo': ['90AA']}>
        headers = {'Authorization': http_autorization}
        url_with_token = 'https://gobiernoabierto.cordoba.gob.ar/api/v2/redbus-data/saldo-tarjeta-redbus/'
        req = requests.post(url_with_token, headers=headers)
        res = req.json()
        # token no es: '{"detail":"Token inv\xc3\xa1lido."}'
        # token OK: b'{"error":"No se incluye el c\xc3\xb3digo de la tarjeta"}'

        ret = False
        if 'error' in res.keys() and res['error'] == "No se incluye el código de la tarjeta":
            # cache de 5h
            cache.set(cache_key, ret, 60 * 60 * 5)
            ret = True

        if 'detail' in res.keys() and res['detail'] == "Token inválido":
            ret = False

        return ret
