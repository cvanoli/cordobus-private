from django.conf.urls import url, include
from django.urls import path
from .algoritmo import distancia_y_tiempo
from rest_framework import routers
from .views import DatosInternosViewSet, ViajeViewSet

 # Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'datos-de-internos', DatosInternosViewSet, base_name='datos-de-internos')
router.register(r'viajes', ViajeViewSet, base_name='viajes')


urlpatterns = [
    url(r'^distancia_y_tiempo/(?P<id_recorrido>[0-9]+)/(?P<id_poste>[0-9]+)$', distancia_y_tiempo, name='tiempo-y-horario'),
    url(r'^', include(router.urls)),
]