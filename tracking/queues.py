class MyQueues():
    ''' colas que descartan elementos al llegar a su capacidad máxima
        Para implementar contando las ultimas velocidades y posiciones '''
    
    def __init__(self, largo=10):
        self.largo = largo
        self.listas = {}

    def get_lista(self, sid):
        ''' crear o recuperar lista '''
        if sid not in self.listas.keys():
            self.listas[sid] = []
        
        return self.listas[sid]

    def append(self, sid, elem):
        lista = self.get_lista(sid=sid)
        lista.append(elem)
        if len(lista) > self.largo:
            lista.pop(0)
    
    def min_max(self, sid):
        ''' obtener los extremos de los valores (ver de que tipo son) 
            Devuelve una tupla con min, max'''
        lista = self.get_lista(sid=sid)
        # if len(lista) < self.largo: versión con datos mas estables
        if len(lista) == 0:  # versión para que rápidamente sepamos cuantos bondis activos hay
            return None
        
        return min(lista), max(lista)
    
    def variacion(self, sid):
        ''' diferencia entre máximo y mínimo '''
        mm = self.min_max(sid=sid)
        if mm is None:
            return None
        
        dif = mm[1] - mm[0]
        return abs(dif)
