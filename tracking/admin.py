from django.contrib.gis import admin
from tracking.models import *
import pytz
import datetime
from django.conf import settings


@admin.register(Tracking)
class TrackingAdmin(admin.OSMGeoAdmin):
    #date_hierarchy = 'timestamp'  # Esto enlentece el admin
    list_display = ('timestamp', 'interno_en_recorrido', 'velocidad')
    list_filter = ['interno_en_recorrido__recorrido__linea', ]
    search_fields = ['interno_en_recorrido__recorrido__linea', 'interno_en_recorrido__interno', 'interno_en_recorrido__recorrido']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related(
            'interno_en_recorrido__interno',
            'interno_en_recorrido__recorrido',
            'interno_en_recorrido__recorrido__linea',
        )


@admin.register(InternoGPS)
class InternoAdmin(admin.OSMGeoAdmin):

    def linea_actual(self, obj):
        if obj.recorrido_actual is None:
            return None
        elif obj.recorrido_actual.linea is None:
            return None
        return obj.recorrido_actual.linea.nombre_publico

    def esta_activo(self, obj):
        return obj.esta_activo()

    esta_activo.boolean = True

    list_display = ('codigo_vehiculo', 'id', 'name', 'esta_activo',
                    'ultima_posicion_time', 'linea_actual', 'recorrido_actual',
                    'adaptado', 'piso_bajo', 'articulado', 'ultimo_viaje',
                    'ultimo_recorrido_ok', 'ultimas_velocidades_promedio',
                    'ultimas_variaciones_latitud', 'ultimas_variaciones_longitud')
    list_filter = ['recorrido_actual__linea', 'recorrido_actual',
                   'ultimo_recorrido_ok', 'adaptado', 'piso_bajo', 'articulado']
    search_fields = ['recorrido_actual__linea__nombre_publico', 'name',
                     'recorrido_actual__descripcion_corta']


@admin.register(InternoEnRecorrido)
class InternoEnRecorridoAdmin(admin.OSMGeoAdmin):

    def linea(self, obj):
        if obj.recorrido is None:
            return 'Sin recorrido'
        elif obj.recorrido.linea is None:
            return 'Sin línea'
        else:
            return obj.recorrido.linea.nombre_publico

    list_display = ('interno', 'linea', 'recorrido', 'ultimo_vez')
    list_filter = ['interno', 'recorrido', 'recorrido__linea__nombre_publico']
    search_fields = ['interno__name', 'recorrido__descripcion_corta']


@admin.register(Viaje)
class ViajeAdmin(admin.OSMGeoAdmin):

    def linea(self, obj):
        if obj.recorrido is None:
            return 'Sin recorrido'
        elif obj.recorrido.linea is None:
            return 'Sin línea'
        else:
            return obj.recorrido.linea.nombre_publico
    
    def duracion(self, obj):
        return obj.terminado - obj.iniciado

    list_display = ('linea', 'interno', 'recorrido', 'duracion', 'iniciado', 'terminado', 'score', 'fecha_calculo')
    list_filter = ['recorrido__linea', 'recorrido', 'interno']
    search_fields = ['interno__name', 'recorrido__name']


@admin.register(MomentoRegistroFrecuencia)
class MomentoRegistroFrecuenciaAdmin(admin.OSMGeoAdmin):
    date_hierarchy = 'timestamp'
    list_display = ['id', 'timestamp']
    search_fields = ['id']


@admin.register(RegistroDeFrecuencia)
class RegistroDeFrecuenciaAdmin(admin.OSMGeoAdmin):
    def unidades_reales(self, obj):
        return obj.unidades_circulando.count()

    date_hierarchy = 'momento__timestamp'
    list_display = ['id', 'linea','unidades_reales', 'unidades_esperadas', 'momento']
    search_fields = ['id']
    list_filter = ['linea', 'linea__empresa']


@admin.register(RegistroDeDeslogueadoFrecuencia)
class RegistroDeDeslogueadoFrecuenciaAdmin(admin.OSMGeoAdmin):
    def no_logueados(self, obj):
        return obj.deslogueados.count()

    date_hierarchy = 'momento__timestamp'
    list_display = ['id', 'no_logueados', 'empresa', 'momento']
    search_fields = ['id']
    list_filter = ['empresa']
