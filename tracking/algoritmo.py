from dateutil import tz
from django.core.cache import cache
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.http import require_GET
from math import atan2, cos, radians, sin, sqrt
from rest_framework.decorators import permission_classes, throttle_classes
from rest_framework.throttling import AnonRateThrottle, UserRateThrottle
# Imports nuestros
from lineas.models import Recorrido
from paradas.models import Parada, PosteParada
from tracking.models import InternoGPS
from tracking.permissions import IsAuthenticatedInQhapax



def Errores():
    ''' Manejamos los errores desde el backend '''

    errores = {
                'OK': 'Sin error',
                'RECORRIDO_INEXISTENTE': 'Tenemos problemas para ver este recorrido de colectivo. Estamos trabajando para solucionarlo.',
                'NO_HAY_BONDIS': 'No tenemos registros de algun colectivo en estos momentos.',
                # Por ahora desactivamos el error este
                #'NO_HAY_GPS': 'Todavia no tenemos registros de algun colectivo desde punta de linea hasta tu parada.',
                'POSTE_INEXISTENTE': 'Tenemos problemas para ver este poste. Estamos trabajando para resolverlo.'
              }
    return errores


def get_close_coord(lista, coordenada):
    '''
    Input:
        -lista: lista de tuplas de float. [(x0, y0),(x1, y1),(x2, y2),...,(xn, yn)]
        -coordenada: una lista de 1 tupla (float) con la coordenada a computar. [(xc, yc)]
    Output:
        -tupla de float
    '''

    # Dado una 'lista' de tuplas retornamos la tupla mas cercana a 'coordenada'
    return min(lista, key=lambda c: (c[0] - coordenada[0])**2 + (c[1] - coordenada[1])**2)


def get_trazado_recortado(trazado, bondi_coord, poste_coord):
    '''
    Input:
        -trazado: lista de tuplas de float. [(x0, y0),(x1, y1),(x2, y2),...,(xn, yn)]
        -bondi_coord: tupla con las coordenadas del bondi (float). [(xb, yb)]
        -poste_coord: tupla con las coordenadas del poste (float). [(xp, yp)]
    Output:
        -lista de tuplas de float
    '''

    # No necesitamos todo el trazado, solo necesitamos el tramo desde la ultima posicion
    # conocida del bondi hasta el poste. O tambien desde el principio del trazado al poste

    if len(bondi_coord) == 0:
        # Tomamos el trazado desde el principio hasta el poste_coord
        return trazado[:trazado.index(poste_coord) + 1]

    # Tomamos el trazado desde bondi_coord hasta el poste_coord
    return trazado[trazado.index(bondi_coord):trazado.index(poste_coord) + 1]


def distancia_entre_coord(origin, destination):
    '''
    Input:
        -origin: tupla de coordenada. (xo, yo)
        -destination: tupla de coordenada. (xd, yd)
    Output:
        -un Float
    '''

    # Calculamos la distancia 'haversine' entre origin y destination
    # Doy vuelta las coordenadas.
    lon1, lat1 = origin
    lon2, lat2 = destination
    radius = 6371  # km

    dlat = radians(lat2 - lat1)
    dlon = radians(lon2 - lon1)
    a = (sin(dlat / 2) * sin(dlat / 2) +
         cos(radians(lat1)) * cos(radians(lat2)) *
         sin(dlon / 2) * sin(dlon / 2))
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    d = radius * c

    return d


def dist_total(trazado):
    '''
    Input:
        -trazado: lista de tuplas. [(x0, y0),(x1, y1),(x2, y2),...,(xn, yn)]
    Output:
        -un Int
    '''

    # Calculamos (sumamos) los pares de tuplas y sacamos la distancia total
    dis_tmp = []
    lista_de_pares = list(zip(trazado, trazado[1:]))
    # lista_de_pares = [((x0,y0),(x1,y1)),((x1,y1),(x2,y2)),((x2,y2),(x3,y3)), ..., ...]
    for par_de_tuplas in lista_de_pares:
        dis_tmp.append(distancia_entre_coord(par_de_tuplas[0], par_de_tuplas[1]))

    # Distancia total en km
    distancia_total = sum(dis_tmp)

    return distancia_total


def tiempo(distancia, velocidad_promedio):
    '''
    Input:
        -distancia: Int
    Output:
        -Un Int
    '''

    # Tiempo = distancia / velocidad
    # Por ahora, la velocidad va a ser constante = 30km.

    # Tiempo en minutos. Redondeado.
    #return round((distancia / 30) * 60)
    return round((distancia / velocidad_promedio) * 60)


def retorno_data(error, detalles, lista_resultados, status=200, request=None):
    '''
    Input:
        -error: Int
        -detalles: String
        -lista_resultados: lista con varios objetos. Ver mas abajo.
        -status: Int
        -request: un Objeto request
    Output:
        -un Json
    '''

    # Creamos el objeto a retornar via json
    res = []
    if len(lista_resultados) == 0:
        data = {
            "error": error,
            "detalles": detalles
        }
        res.append(data)
        # Mandamos el json directo desde aca
        return JsonResponse(res, safe=False, status=status)
    else:
        for resultados in lista_resultados:
            data = {
                "error": error,
                "detalles": detalles,
                "resultados": {
                    'ya_paso': resultados[0],
                    'numero_interno': resultados[1],
                    'hora_ultima_posicion': resultados[2],
                    'longitud': resultados[3][0],
                    'latitud': resultados[3][1],
                    'piso_bajo': resultados[4],
                    'adaptado': resultados[5],
                    'articulado': resultados[6],
                    'distancia_km': resultados[7],
                    'tiempo_minutos': resultados[8]
                }
            }
            res.append(data)

    return JsonResponse(res, safe=False, status=status)


@cache_page(10) # 10 segundos de cache
@require_GET # Solo peticiones GET
@throttle_classes([AnonRateThrottle, UserRateThrottle]) # Definidas en setting
@permission_classes((IsAuthenticatedInQhapax, )) # Definido en permissions
def distancia_y_tiempo(request, id_recorrido, id_poste):
    '''
    Input:
        -request: objeto request
        -id_recorrido: id del recorrido
        -id_poste: id del poste
    Output:
        -un Json
    '''

    # Usamos los errores predefinidos desde el backend. Diccionario de errores
    errores = Errores()
    # Identificamos nuestro recorrido
    try:
        # Matcheamos con el id de la base de cordobus
        recorrido = Recorrido.objects.get(id=id_recorrido, publicado=True)
    except Recorrido.DoesNotExist:
        # No deberiamos entrar nunca aca, ya que el front consulta API y nos da el id que SI existe
        respuesta = retorno_data(error=1, detalles=errores['RECORRIDO_INEXISTENTE'], lista_resultados=[],
                                    status=200, request=request)
        return respuesta

    # Obtenemos el recorrido en sí de ese id. A partir de ahora: 'trazado'.
    recorrido_trazado = recorrido.trazado_extendido.coords

    # Me fijo si hay internos activos en las ultimas 2 horas.
    # Si no, deberiamos avisarle al usuario que no tenemos datos frescos de su busqueda
    internos_activos = InternoGPS.objects.activos_ahora(seconds=7200)

    # Obtenemos todos los internos activos asociado al recorrido dado.
    internos = internos_activos.filter(recorrido_actual=recorrido)
    # Caigo en el if si internos = QuerySet vacio
    if not internos.exists():
        respuesta = retorno_data(error=2, detalles=errores['NO_HAY_BONDIS'], lista_resultados=[],
                                    status=200, request=request)
        return respuesta

    try:
        # Obtenemos las coordenadas reales del poste
        real_poste_coor = PosteParada.objects.get(id=id_poste).ubicacion.coords
    except PosteParada.DoesNotExist:
        # No deberiamos entrar nunca aca, el poste lo obtiene previamente desde el endpoint
        respuesta = retorno_data(error=4, detalles=errores['POSTE_INEXISTENTE'], lista_resultados=[],
                                    status=200, request=request)
        return respuesta

    # Las coordenas del poste pueden no ser parte del trazado. El punto del trazado que
    # este mas cerca del poste sera tomado como coordenada del poste. Por eso es fake.
    # (Hacemos al poste parte del trazado)
    fake_poste_coor = get_close_coord(lista=recorrido_trazado, coordenada=real_poste_coor)

    # Ahora calculamos la distancia entre la coordenada real del poste y la fake
    # Este resultado lo vamos a tener en cuenta para sumarlo al final
    dist_real_entre_postes = distancia_entre_coord(origin=fake_poste_coor, destination=real_poste_coor)

    # Trazado desde el principio del recorrido hasta el poste
    poste_trazado = get_trazado_recortado(trazado=recorrido_trazado, bondi_coord=(), poste_coord=fake_poste_coor)

    # Guardo los bondis que todavia no pasaron por mi poste
    bondis_anterior_poste = []
    # Guardo los bondis que ya pasaron por mi poste para mostrarlo de otra forma
    bondis_siguientes_poste = []


    for interno in internos:
        # Convertimos las coordenadas del interno en una lista para el get_close_coord
        tmp_bondi_trazado = list(interno.ultima_posicion_conocida.coords)
        # Convertimos las coordenadas del bondi en trazado
        bondi_trazado = get_close_coord(lista=recorrido_trazado, coordenada=tmp_bondi_trazado)
        # Verificamos que bondi_trazado este contenido en poste_trazado
        if bondi_trazado in poste_trazado:
            # Si todavia no paso por mi parada (poste), entonces lo guardo.
            bondis_anterior_poste.append(interno)
        else:
            # Si ya paso por mi parada (poste), entonces lo guardo para mostrarlo de otra forma.
            bondis_siguientes_poste.append(interno)

    # Definimos una  vez el utc para luego usarlo con todos los internos
    tz_arg = tz.tzstr('UTC-3')

    bondis_no_pasaron = []
    for interno in bondis_anterior_poste:
        # Convertimos las coordenadas del interno en lista para el get_close_coord
        real_bondi_coor = list(interno.ultima_posicion_conocida.coords)

        # Primero convertimos al bondi en parte de nuestro trazado
        fake_bondi_coor = get_close_coord(lista=recorrido_trazado, coordenada=real_bondi_coor)
        # Luego, obtenemos el trazado desde el bondi hasta el poste
        trazado_recortado = get_trazado_recortado(trazado=recorrido_trazado, bondi_coord=fake_bondi_coor, poste_coord=fake_poste_coor)

        # Calculamos la distancia entre las coordenadas reales del bondi y las fake
        # para tenerla en cuenta en el resultado final
        dist_real_entre_bondis = distancia_entre_coord(origin=fake_bondi_coor, destination=real_bondi_coor)

        # Calculamos la distancia total del trazado_recortado
        dis_total_tmp = dist_total(trazado=trazado_recortado) + dist_real_entre_postes + dist_real_entre_bondis
        # Redondeamos a 1 decimal
        dis_total = round(dis_total_tmp, 1)
        # Calculamos el tiempo de la distancia total que tenemos con velocidad constante = 25km
        tiem = tiempo(distancia=dis_total, velocidad_promedio=float(interno.ultimas_velocidades_promedio))
        # Vamos a devolver la fecha en formato UTC-3 (ARG)
        ult_pos_en_utc_arg = interno.ultima_posicion_time.astimezone(tz_arg)
        ult_pos_time = ult_pos_en_utc_arg.strftime("%H:%M")
        # Vamos a devolver [True, num_interno, HH:MM, (xcor1, ycor1), pb1, adapt1, art1, dist1, tiem1], [num_interno2, (xcor2, ycor2), pb1, adapt1, art1, dist2, tiem2], ...
        # NOTA: el False hardcodeado es para marcar que el bondi todavia no paso por el poste seleccionado
        bondis_no_pasaron.append([False, interno.name, ult_pos_time,
                                interno.ultima_posicion_conocida.coords,
                                interno.piso_bajo, interno.adaptado,
                                interno.articulado, dis_total, tiem])

    # Vamos a crear otra lista respetando los parametros que necesita la funcion retorno_data()
    bondis_ya_pasaron = []
    # Si bondis_siguientes_poste es vacio el for no se deberia ejecutar
    for interno in bondis_siguientes_poste:
        # Vamos a devolver la fecha en formato UTC-3 (ARG) de los bondis que ya pasaron
        ult_pos_en_utc_arg = interno.ultima_posicion_time.astimezone(tz_arg)
        ult_pos_time = ult_pos_en_utc_arg.strftime("%H:%M")
        # Ver el json que devolvenos desde la funcion retorno_data para entender los parametros
        # NOTA: el True hardcodeado es para marcar que el bondi ya paso por el poste seleccionado
        bondis_ya_pasaron.append([True, interno.name, ult_pos_time,
                                interno.ultima_posicion_conocida.coords,
                                interno.piso_bajo, interno.adaptado,
                                interno.articulado, 0, 0])

    # Vamos a pasar bondis_no_pasaron ordenados por distancia.
    res_ordenado = sorted(bondis_no_pasaron, key=lambda k: k[7])

    # Concatenamos todos los bondis para mostrar via api
    # Nota: si bondis_ya_pasaron es vacio entonces lista_final = bondis_no_pasaron
    lista_final = res_ordenado + bondis_ya_pasaron

    # Pasamos la lista_resultados con todos los datos de los internos
    respuesta = retorno_data(error=0, detalles=errores['OK'], lista_resultados=lista_final,
                                status=200, request=request)
    return respuesta
