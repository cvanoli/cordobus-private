from django.contrib.gis.db import models
import logging
import pytz
import datetime
from django.conf import settings
from lineas.models import Empresa, Recorrido, Linea
from colectivos.models import Interno
from tracking.managers import InternoGPSManager, TrackingManager
# from django_pandas.managers import DataFrameManager
logger = logging.getLogger(__name__)


class InternoGPS(models.Model):
    """
    Datos que llegan vía GPS de cada unidad de colectivos
    """
    name = models.CharField("Nombre", blank=True, null=True, max_length=30)
    codigo_vehiculo = models.CharField(max_length=12, null=True, blank=True)
    # datos de la última posicion reportada. Posdría ser un elemento tracking pero esto es mas rápido
    empresa = models.ForeignKey(Empresa, null=True, blank=True, on_delete=models.SET_NULL)
    ultima_posicion_conocida = models.PointField(blank=True, null=True)
    ultima_posicion_time = models.DateTimeField(blank=True, null=True)
    recorrido_actual = models.ForeignKey('lineas.Recorrido', on_delete=models.SET_NULL, blank=True, null=True, related_name='recorridos_actuales')
    # ultimo recorrido que se encontro OK (no nulo - logueado)
    ultimo_recorrido_ok = models.ForeignKey('lineas.Recorrido', on_delete=models.SET_NULL, blank=True, null=True, related_name='ultimos_recorridos_conocidos')

    ultimas_velocidades_promedio = models.DecimalField(max_digits=6, decimal_places=2, default=0.0,
                                                        help_text='Velocidades promedio segun ultimos movimientos')

    # para saber si se mueve! a veces nos informa velocidad PERO no se mueve
    ultimas_variaciones_latitud = models.DecimalField(max_digits=11, decimal_places=8, default=0.0,
                                                        help_text='Variacion en latitud (geo) en los ultimos minutos')
    ultimas_variaciones_longitud = models.DecimalField(max_digits=11, decimal_places=8, default=0.0,
                                                        help_text='Variacion en longitud (geo) en los ultimos minutos')

    adaptado = models.BooleanField(default=False, help_text='La unidad esta adaptada para poder cargar sillas de ruedas')
    piso_bajo = models.BooleanField(default=False, help_text='La unidad tiene el piso bajo y permite que suban sillas de ruedas')
    articulado = models.BooleanField(default=False, help_text='La unidad es tipo "gusano" y tiene dos partes articuladas')

    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    ultimo_viaje = models.ForeignKey('Viaje', on_delete=models.SET_NULL, null=True, blank=True, related_name='interno_actual')

    objects = InternoGPSManager()

    def tiempo_sin_tracking(self):
        desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now())
        return desde - self.ultima_posicion_time

    def esta_activo(self):
        activo = True

        desde = datetime.timedelta(seconds=settings.SEGUNDOS_SIN_TRACKING_PARA_CONSIDERAR_ACTIVO)
        if self.tiempo_sin_tracking() > desde:
            activo = False

        # ver si registra velocidad
        if self.ultimas_velocidades_promedio < 1.0:
            activo = False

        # ver si se mueve
        if self.ultimas_variaciones_latitud < 0.001 or self.ultimas_variaciones_longitud < 0.001:
            activo = False

        return activo

    def full_name(self):
        ret = ''
        if self.recorrido_actual is None:
            ret = 'No registrado'
        else:
            ret = '{}:{}'.format(self.recorrido_actual.linea.name, self.recorrido_actual.name)

        return '{} {}'.format(self.name, ret)

    def linea_name(self, default='No registrado'):
        ''' version segura de un string para la línea '''
        if self.recorrido_actual is None:
            return default
        elif self.recorrido_actual.linea is None:
            return default
        else:
            return self.recorrido_actual.linea.name

    def linea_id(self, default=0):
        ''' version segura de un string para la línea '''

        if self.recorrido_actual is None:
            return default
        elif self.recorrido_actual.linea is None:
            return default
        else:
            return self.recorrido_actual.linea.id

    class Meta:
        verbose_name = "Interno"
        verbose_name_plural = "Internos"
        ordering = ("name", )

    def __str__(self):
        return self.name or 'Desconocido'


### ESTA CLASE LA BORRARIA PARA DEJAR UNICAMENTE VIAJE
class InternoEnRecorrido(models.Model):
    ''' toda vez que un colectivo (interno) hace un recorrido específico '''
    interno = models.ForeignKey(InternoGPS, related_name='recorridos', on_delete=models.SET_NULL, blank=True, null=True)
    recorrido = models.ForeignKey('lineas.Recorrido', related_name='internos', on_delete=models.SET_NULL, blank=True, null=True)
    ultimo_vez = models.DateTimeField(null=True, blank=True)

    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    # objects = InternoEnRecorridoManager()

    def __str__(self):
        return '{} en {}'.format(self.interno, self.recorrido)


class Viaje(models.Model):
    """ cada uno de los viajes de un interno en un recorrido """
    interno = models.ForeignKey(InternoGPS, related_name='viajes', on_delete=models.SET_NULL, blank=True, null=True)
    recorrido = models.ForeignKey('lineas.Recorrido', related_name='viajes', on_delete=models.SET_NULL, blank=True, null=True)

    iniciado = models.DateTimeField(auto_now=False, auto_now_add=True)
    terminado = models.DateTimeField(null=True, blank=True)

    score = models.FloatField(null=True, blank=True, default=None)
    fecha_calculo = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '{} en {}: {}'.format(self.interno, self.recorrido, self.iniciado)


class Tracking(models.Model):
    """ cada uno de los puntos donde las unidades reportan ubicación y velocidad """
    interno_en_recorrido = models.ForeignKey(InternoEnRecorrido, related_name='trackings', on_delete=models.SET_NULL, null=True, blank=True)
    velocidad = models.IntegerField("Velocidad", blank=True)
    location = models.PointField(null=True, spatial_index=True, geography=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    # lo dejo aqui porque un recorrido/linea podría cambiar de empresa a futuro
    viaje = models.ForeignKey(Viaje, on_delete=models.SET_NULL, null=True, blank=True)

    objects = TrackingManager()
    # no funciono como esperábamos objects = DataFrameManager()

    class Meta:
        verbose_name = "Tracking"
        verbose_name_plural = "Tracking"
        ordering = ("-timestamp", )  # NO CAMBIAR
        indexes = [
            models.Index(fields=['-timestamp']),
            models.Index(fields=['-timestamp', '-id']),
            models.Index(fields=['timestamp']),
        ]

    def __str__(self):
        return 'Track {}'.format(self.id)


class MomentoRegistroFrecuencia(models.Model):
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)


    class Meta:
        verbose_name = 'Momento del registro de frecuencia'
        verbose_name_plural = 'Momentos del registro de frecuencia'
        ordering = ('-timestamp',)


class RegistroDeFrecuencia(models.Model):
    momento = models.ForeignKey(MomentoRegistroFrecuencia, on_delete=models.CASCADE,
        related_name='registrosdefrecuencias')
    linea = models.ForeignKey(Linea, related_name='registrosdefrecuencias',
        on_delete=models.CASCADE)
    unidades_circulando = models.ManyToManyField(Interno, blank=True)
    unidades_esperadas = models.IntegerField()

    @property
    def cantidad_unidades_circulando(self):
        """
        Retorna la cantidad de internos circulando
        """
        return self.unidades_circulando.count()

    @property
    def colectivos_faltantes(self):
        """
        Indica la cantidad de unidades que faltan para cumplir con la
        frecuencia esperada. Si circulan más colectivos de los que esperamos
        se retorna 0.
        """
        diferencia = self.unidades_esperadas - self.cantidad_unidades_circulando
        resultado =  diferencia if diferencia > 0 else 0
        return resultado

    class Meta:
        verbose_name = 'Registro de frecuencia'
        verbose_name_plural = 'Registros de frecuencias'


class RegistroDeDeslogueadoFrecuencia(models.Model):
    momento = models.ForeignKey(MomentoRegistroFrecuencia, on_delete=models.CASCADE,
        related_name='registrosdeslogueados')
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE,
        related_name='registrosdeslogueados')
    deslogueados = models.ManyToManyField(Interno, blank=True)

    class Meta:
        verbose_name = 'Registro de deslogueado'
        verbose_name_plural = 'Deslogueados'
