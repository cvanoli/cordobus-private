GTFS
====

*GTFS* es el estándar a través del cuál Google publica los datos del sistema de transporte de las ciudades. La Ciudad de Córdoba cuenta con datos que se actualizan periodicamente listos para ser publicados por Google.


Actualización de los archivos que componen GTFS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
GTFS pide archivos obligatorios que permiten la publicación de los datos en su plataforma.
Para actualizar los datos de los archivos para GTFS:
1) Borrar el archivo que se quiere actualizar en el servidor.
2) Ejecutar el comando:

.. code:: bash 

  python manage.py generar_gtfs
