from django.core.management.base import BaseCommand
from lineas.models import Empresa, Linea, Troncal, Recorrido
from lineas import settings as settings_tp
from django.db import transaction
import sys
import requests
import json


def crear_troncales():
    print('Creando lineas troncales..')

    troncales = ['10', '20', '30', '40', '50', '60', '70', '80', 'BARRIALES', 'AEROPUERTO', '600', 'TROLEBUSES']
    troncal_nuevo = 0
    troncal_repetido = 0

    #creamos o agregamos los troncales
    for troncal in troncales:
        t, created = Troncal.objects.get_or_create(nombre_publico=troncal)
        if created:
            troncal_nuevo += 1
        else:
            troncal_repetido += 1

    # asignamos los troncales a las lineas publicadas
    lineas = Linea.objects.filter(publicado=True)
    for linea in lineas:
        if linea.nombre_publico in ['600', '601']:
            linea.troncal = Troncal.objects.get(nombre_publico='600')
        elif linea.nombre_publico in ['B20', 'B30', 'B50', 'B60', 'B61', 'B70']:
            linea.troncal = Troncal.objects.get(nombre_publico='BARRIALES')
        elif linea.nombre_publico in ['A', 'B', 'C']:
            linea.troncal = Troncal.objects.get(nombre_publico='TROLEBUSES')
        elif linea.nombre_publico == 'AEROBUS':
            linea.troncal = Troncal.objects.get(nombre_publico='AEROPUERTO')
        else:
            if linea.nombre_publico.startswith('1'):
                linea.troncal = Troncal.objects.get(nombre_publico='10')
            elif linea.nombre_publico.startswith('2'):
                linea.troncal = Troncal.objects.get(nombre_publico='20')
            elif linea.nombre_publico.startswith('3'):
                linea.troncal = Troncal.objects.get(nombre_publico='30')
            elif linea.nombre_publico.startswith('4'):
                linea.troncal = Troncal.objects.get(nombre_publico='40')
            elif linea.nombre_publico.startswith('5'):
                linea.troncal = Troncal.objects.get(nombre_publico='50')
            elif linea.nombre_publico.startswith('6'):
                linea.troncal = Troncal.objects.get(nombre_publico='60')
            elif linea.nombre_publico.startswith('7'):
                linea.troncal = Troncal.objects.get(nombre_publico='70')
            elif linea.nombre_publico.startswith('8'):
                linea.troncal = Troncal.objects.get(nombre_publico='80')
        linea.save()

    print('FIN. {} troncales nuevos, {} troncales repetidos'.format(troncal_nuevo, troncal_repetido))


class Command(BaseCommand):
    help = """Comando para Importar empresas y líneas de transporte desde webservice del proveedor"""

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación/actualización de empresas y líneas de transporte de una ciudad'))

        url = settings_tp.URL_BASE_WS_PROVEEDOR+'lineas'
        self.stdout.write(self.style.SUCCESS('Conectando a  {}'.format(url)))

        empresa_nueva = 0
        empresa_repetida = 0
        linea_nueva = 0
        linea_repetida = 0

        # el ciclo es para procesar todas las páginas del api
        while True:
            try:
                response = requests.get(url, timeout=25.0)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERROR trayendo datos: {} \n'.format(e)))
                sys.exit(1)

            json_data = json.loads(response.text)
            results_json = json_data['results']
            url = json_data['next']

            for dato in results_json:
                # dato
                # {'id': 309,
                #   'id_externo': 1329,
                #   'nombre_publico': '70',
                #   'empresa': 'ERSA',
                #   'id_empresa': '6'}
                empresa, created = Empresa.objects.get_or_create(nombre_publico=dato['empresa'])

                if not created:
                    empresa_repetida += 1
                else:
                    empresa_nueva += 1

                linea, created = Linea.objects.get_or_create(
                                                    empresa=empresa,
                                                    nombre_publico=dato['nombre_publico'])

                if not created:
                    linea_repetida += 1
                else:
                    linea_nueva += 1

                linea.id_externo = dato['id_externo']
                linea.save()
                self.stdout.write(self.style.SUCCESS('Linea: {}'.format(linea)))

            if url is None:
                break

        # las lineas las agrupamos por troncales para el despliegue de opciones de Go!
        crear_troncales()

        self.stdout.write(self.style.SUCCESS('FIN. {} empresas nuevas'.format(empresa_nueva)))
        self.stdout.write(self.style.SUCCESS('FIN. {} lineas nuevas. {} lineas actualizadas'.format(linea_nueva, linea_repetida)))
