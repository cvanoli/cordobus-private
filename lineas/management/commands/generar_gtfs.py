from django.core.management.base import BaseCommand
from lineas.gtfs.views_agency import gen_agency_txt
from lineas.gtfs.views_routes import gen_routes_txt
from lineas.gtfs.views_shapes import gen_shapes_txt
from lineas.gtfs.views_stop_times import gen_stop_times_txt
from lineas.gtfs.views_stops import gen_stops_txt
from lineas.gtfs.views_trips import gen_trips_txt
from lineas.gtfs.views_feed_info import gen_feed_info_txt
from lineas.gtfs.views_calendar import gen_calendar_txt
from lineas.gtfs.views_frequencies import gen_frequencies_txt
from django.conf import settings
import zipfile
import os


class Command(BaseCommand):
    help = """Generar los archivos del GTFS y comprimirlos"""

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Creando GTFS ...'))

        gtfs_files = {'agency.txt': gen_agency_txt,
                        'routes.txt': gen_routes_txt,
                        'shapes.txt': gen_shapes_txt,
                        'stops.txt': gen_stops_txt,
                        'stop_times.txt': gen_stop_times_txt,
                        'trips.txt': gen_trips_txt,
                        'feed_info.txt': gen_feed_info_txt,
                        'calendar.txt': gen_calendar_txt,
                        'frequencies.txt': gen_frequencies_txt}

        dest_file_name = 'gtfs-cordoba.zip'
        archivo_comprimido_path = os.path.join(settings.MEDIA_ROOT, dest_file_name)
        archivo_comprimido = zipfile.ZipFile(archivo_comprimido_path, 'w')

        for gfile in gtfs_files.keys():
            self.stdout.write(self.style.SUCCESS('Generating: {} ...'.format(gfile)))
            pfile = os.path.join(settings.MEDIA_ROOT, 'gtfs', gfile)
            exists = os.path.isfile(pfile)
            if not exists:
                func = gtfs_files[gfile]
                func()
            archivo_comprimido.write(pfile, gfile, compress_type = zipfile.ZIP_DEFLATED)

        self.stdout.write(self.style.SUCCESS('Comprimicionando'))
        archivo_comprimido.close()
        
        self.stdout.write(self.style.SUCCESS('FIN'))
