from django.core.management.base import BaseCommand
from lineas.models import Recorrido, Linea
from lineas import settings as settings_tp
from django.db import transaction
import sys
import requests
import json
from django.contrib.gis.geos import LineString


class Command(BaseCommand):
    help = """Comando para importar recorridos de transporte desde webservice del proveedor"""

    # @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando actualizacion de recorridos de transporte'))

        url = settings_tp.URL_BASE_WS_PROVEEDOR_GA+'recorridos-colectivos/'

        nuevos_recorridos = 0
        repetidos_recorridos = 0

        recorridos_cargados = []
        puntos_de_recorridos = {} # diccionario para armar el trazado de los recorridos
        while True:

            try:
                response = requests.get(url, timeout=15.0)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERROR trayendo datos: {}'.format(e)))
                continue

            result_json = json.loads(response.text)
            url = result_json['next']
            results = result_json['results']

            for feature in results['features']:
                propiedades = feature['properties']
                id_recorrido = feature['id']
                try:
                    linea = Linea.objects.get(nombre_publico=propiedades['linea']['nombre_publico'])
                except Exception as e:
                    self.stdout.write(self.style.ERROR('ERROR AL OBTENER LINEA: {}'.format(er)))
                    # no debería suceder nunca este error
                    sys.exit(1)

                recorrido, created = Recorrido.objects.get_or_create(linea=linea,
                                        descripcion_corta=propiedades['descripcion_corta'],
                                        publicado=True)
                recorrido.trazado = LineString(feature['geometry']['coordinates'])
                recorrido.id_externo = propiedades['id_externo']
                recorrido.descripcion_larga = propiedades['descripcion_larga']
                recorrido.descripcion_interna = propiedades['descripcion_cuando_llega']
                recorrido.save()

                if created:
                    nuevos_recorridos += 1
                else:
                    repetidos_recorridos += 1

                self.stdout.write(self.style.SUCCESS('Recorrido {}'.format(recorrido)))

            if url is None:
                break

        self.stdout.write(self.style.SUCCESS('******\n FIN \n****** \n'))

        nuevos_recorridos_str = '{} recorridos nuevos'.format(nuevos_recorridos)
        repetidos_recorridos_str = '{} recorridos repetidos'.format(repetidos_recorridos)

        self.stdout.write(self.style.SUCCESS('{}. {}.'.format(nuevos_recorridos_str, repetidos_recorridos_str)))
