from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.gis.db import models
from django.contrib.gis.geos import LineString



class Empresa(models.Model):
    """Cada una de las empresas proveedores del servicio"""
    # ID en el sistema externo(en este caso en el GPS)
    id_externo = models.CharField(max_length=90, null=True, blank=True, help_text='ID en el sistema externo si hubiera un provedor')
    nombre_publico = models.CharField(
        max_length=250, null=True, blank=True,
        help_text='Nombre de fantasía o como la conoce el público general'
    )
    color = models.CharField(null=True, blank=True, max_length=250,
        help_text='Color de referencia de la empresa. Sirve para unificar colores de gráficos')

    # relación opcional a otro modelo cualquiera que es
    # "dueño" (persona juridica)
    organizacion_content_type = models.ForeignKey(ContentType, blank=True,
                                                  null=True,
                                                  on_delete=models.SET_NULL)
    organizacion_object_id = models.PositiveIntegerField(blank=True, null=True)
    organizacion = GenericForeignKey('organizacion_content_type',
                                     'organizacion_object_id')

    url = models.URLField(null=True, blank=True, help_text='URL de la empresa')
    telefono = models.CharField(null=True, blank=True, max_length=25, help_text='Telefono de la empresa')
    email = models.EmailField(null=True, blank=True, help_text='Telefono de la empresa')
    
    # Para tener registro de futuras modificaciones
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre_publico or ''

    class Meta:
        ordering = ['nombre_publico']


class Troncal(models.Model):
    """
    Conjunto de lineas del transporte publico
    """
    nombre_publico = models.CharField(max_length=50)

    def __str__(self):
        return 'Troncal {}'.format(self.nombre_publico)

    class Meta:
        ordering = ['nombre_publico']
        verbose_name_plural = 'Troncales'


class Linea(models.Model):
    """
    Cada una de las lineas de transporte publico masivo
    """
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, null=True, blank=True)
    troncal = models.ForeignKey(Troncal, on_delete=models.SET_NULL, related_name='lineas', blank=True, null=True)
    id_externo = models.CharField(max_length=45, null=True, blank=True, help_text='ID en el sistema externo si hubiera un provedor')
    nombre_publico = models.CharField(max_length=50)
    tiempo_de_recorrido = models.IntegerField(help_text='Minutos que demora un interno en hacer el recorrido ida y vuelta.',
        null=True, blank=True)

    # Agregamos el campo publicado por default en True. En False quiere decir que esta deshabilitado temporalmente.
    publicado = models.BooleanField(default=True, help_text='Para desactivar temporalmente cuando sea necesario')

    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    # objects = LineaManager()

    def __str__(self):
        return '{} ({})'.format(
            self.nombre_publico, self.empresa.nombre_publico or 'Desconocido'
        )

    class Meta:
        ordering = ['empresa', 'nombre_publico']
        verbose_name = "Linea"
        verbose_name_plural = "Lineas"


class Recorrido(models.Model):
    """ Cada uno de los recorridos diferentes de una linea de transporte publico """
    linea = models.ForeignKey(Linea, on_delete=models.CASCADE, related_name='recorridos')
    id_externo = models.CharField(max_length=90, null=True, blank=True, help_text='ID en el sistema externo si hubiera un provedor')
    # quizas se puedan eliminar, venían de un proveedor original
    descripcion_corta = models.CharField(max_length=90, null=True, blank=True, help_text='viene desde proveedor')
    descripcion_larga = models.CharField(max_length=255, null=True, blank=True, help_text='viene desde proveedor')
    trazado = models.LineStringField(
        null=True, blank=True,
        help_text='Linea que representa el trazado del recorrido de una linea\
         de transporte'
    )
    descripcion_interna = models.TextField(null=True, blank=True, help_text='descripción más amplia del recorrido agregada manualmente')

    publicado = models.BooleanField(default=True, help_text='Para desactivar temporalmente cuando sea necesario')

    # la funcion split_linestring _desgrana_ en recorrido en puntos más cercanos útiles para muchas cosas
    trazado_extendido = models.LineStringField(null=True, blank=True, help_text='trazado con mas puntos intermedios para mejor análisis')

    def __str__(self):
        return '{} {}'.format(self.linea.nombre_publico, self.descripcion_corta)

    def save(self, *args, **kwargs):
        te = self.get_trazado_extendido()
        if te is not None:
            self.trazado_extendido = te
        super().save(*args, **kwargs)

    def get_media_point(self, p1, p2):
        # obtener el punto medio entre otros dos
        x = p1[0] + ((p2[0] - p1[0]) / 2)
        y = p1[1] + ((p2[1] - p1[1]) / 2)

        punto_nuevo = (x, y)
        linea_tmp = LineString(p1, punto_nuevo)
        nueva_distancia = round(linea_tmp.length, 4)

        return punto_nuevo, nueva_distancia

    def split_linestring(self, p1, p2, max_length_line, level=0, ret=[]):
        # achicar un punto hasta que ande
        linea_tmp = LineString(p1, p2)
        if p1 not in ret:
            ret.append(p1)

        if linea_tmp.length > max_length_line:
            punto_nuevo, nueva_distancia = self.get_media_point(p1, p2)

            if nueva_distancia < max_length_line:
                ret.append(punto_nuevo)
            else:
                # las dos partes deben seguirse dividiendo
                ret1 = self.split_linestring(
                    p1=p1, p2=punto_nuevo,
                    max_length_line=max_length_line,
                    level=level + 1,
                    ret=[]
                )
                ret2 = self.split_linestring(
                    p1=punto_nuevo, p2=p2,
                    max_length_line=max_length_line,
                    level=level + 1,
                    ret=[]
                )
                ret += ret1[1:]
                ret += ret2[1:]

        if p2 not in ret:
            ret.append(p2)
        return ret

    def get_trazado_extendido(self):
        max_length_line = 0.001

        if self.trazado is None:
            return None
        puntos_final = []
        c = 0
        agregados = 0
        for punto in self.trazado:
            if c == 0:
                puntos_final.append(punto)
                punto_anterior = punto
                c += 1
                continue

            # ver si hay que cortarlo:
            ret = self.split_linestring(p1=punto_anterior, p2=punto,
                                        max_length_line=max_length_line,
                                        level=0,
                                        ret=[])
            puntos_final += ret[1:]
            punto_anterior = punto
            c += 1

        return LineString(puntos_final)


    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    # objects = RecorridoManager()

    class Meta:
        ordering = ("linea", "descripcion_corta" )
        unique_together = (("id_externo", "linea"),)
