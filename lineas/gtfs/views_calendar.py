"""
Specs: https://developers.google.com/transit/gtfs/reference/?hl=es-419#stopstxt
"""
import django_excel as excel
from colectivos.models import FranjaHorariaFrecuencias
from django.conf import settings
import os
from django.views.decorators.cache import cache_page
import shutil


def gen_calendar_txt(dest_file='calendar'):
    csv_list = []
    headers = ['service_id', 'monday', 'tuesday', 'wednesday', 'thursday',
                'friday', 'saturday', 'sunday', 'start_date', 'end_date'
                ]
    csv_list.append(headers)

    horarios = FranjaHorariaFrecuencias.objects.all()

    for horario in horarios:

        lst = [horario.id,
                int(horario.agrupador.dias.lunes), # lunes
                int(horario.agrupador.dias.martes), # martes
                int(horario.agrupador.dias.miercoles), # miércoles
                int(horario.agrupador.dias.jueves), # jueves
                int(horario.agrupador.dias.viernes), # viernes
                int(horario.agrupador.dias.sabado), # sábado
                int(horario.agrupador.dias.domingo), # domingo
                '20190101', # start_date
                '20191231' # end_date
                ]

        csv_list.append(lst)

    # guardar el archivo para zomprimir y futuras referencias
    dest_fold = os.path.join(settings.MEDIA_ROOT, 'gtfs')
    if not os.path.exists(dest_fold):
        os.makedirs(dest_fold)
    dest_csv = os.path.join(dest_fold, dest_file + '.csv')
    sheet = excel.pe.Sheet(csv_list)
    sheet.save_as(filename=dest_csv)

    shutil.copyfile(dest_csv, dest_csv.replace('.csv', '.txt'))

    return  sheet

@cache_page(60 * 60 * 24)  # 1 h
def gtfs_calendar(request):
    '''
    routes.txt from GTFS spec
    '''
    sheet = gen_calendar_txt()
    return excel.make_response(sheet, 'csv')