"""

Specs: https://developers.google.com/transit/gtfs/reference/?hl=es-419#stopstxt


"""
import django_excel as excel
from paradas.models import Parada, PosteParada
from django.conf import settings
import os
from django.views.decorators.cache import cache_page
import shutil


def gen_stops_txt(dest_file='stops'):
    csv_list = []
    headers = ['stop_id', 'stop_code', 'stop_name', 'stop_desc', 'stop_lat',
                'stop_lon', 'zone_id', 'stop_url', 'location_type',
                'parent_station', 'stop_timezone', 'wheelchair_boarding'
                # 'platform_code'
                ]
    csv_list.append(headers)

    # EL ORDEN DE LAS PARADAS ES MUY IMPORTANTRE
    # paradas = Parada.objects.filter(recorrido__publicado=True, recorrido__linea__publicado=True).order_by('poste__id')
    postes = PosteParada.objects.filter(paradas__verificado=True).distinct()

    for poste in postes:

        codigo = poste.identificador or poste.descripcion or poste.id
        lst = [poste.id, codigo, codigo, poste.descripcion,
                poste.ubicacion.y,
                poste.ubicacion.x,
                '',  # si hay zonas con precios diferenciados acá debe ir el ID de la zona
                '',  # URL con info de la parada si la hubiera
                0,  # tipo de ubicacion: 0=Parada; 1=Estacion que incluye muchas paradas, 2=entrada o salida de estacion
                '',  # parent_station
                '',  # stop_timezone
                0,  # wheelchair_boarding: 0=no se, 1=joya para subir sillas de ruedas; 2=no se pueda
                # ''   platform_code
                ]

        csv_list.append(lst)

    # guardar el archivo para zomprimir y futuras referencias
    dest_fold = os.path.join(settings.MEDIA_ROOT, 'gtfs')
    if not os.path.exists(dest_fold):
        os.makedirs(dest_fold)
    dest_csv = os.path.join(dest_fold, dest_file + '.csv')
    sheet = excel.pe.Sheet(csv_list)
    sheet.save_as(filename=dest_csv)

    shutil.copyfile(dest_csv, dest_csv.replace('.csv', '.txt'))

    return sheet

    
@cache_page(60 * 60 * 24)  # 1 h
def gtfs_stops(request):
    '''
    stops.txt from GTFS spec
    '''
    sheet = gen_stops_txt()
    return excel.make_response(sheet, 'csv')