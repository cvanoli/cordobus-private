from django.urls import path
from django.conf.urls import url
from django.conf.urls import include

#vistas
from lineas.gtfs.views import gtfs_zip_file
from lineas.gtfs.views_agency import gtfs_agency
from lineas.gtfs.views_stops import gtfs_stops
from lineas.gtfs.views_routes import gtfs_routes
from lineas.gtfs.views_trips import gtfs_trips

from lineas.gtfs.views_stop_times import gtfs_stop_times
#from lineas.gtfs.views_calendar import gtfs_calendar, gtfs_calendar_dates
#from lineas.gtfs.views_ import gtfs_
from lineas.gtfs.views_shapes import gtfs_shapes
from lineas.gtfs.views_feed_info import gtfs_feed_info
from lineas.gtfs.views_frequencies import gtfs_frequencies
from lineas.gtfs.views_calendar import gtfs_calendar


urlpatterns = [
    
    url(r'^gtfs-cordoba.zip$', gtfs_zip_file, name='gtfs-zip'),
    # Obligatorios
    url(r'^agency.txt$', gtfs_agency, name='gtfs-agency'),
    url(r'^stops.txt$', gtfs_stops, name='gtfs-stops'),
    url(r'^routes.txt$', gtfs_routes, name='gtfs-routes'),
    url(r'^trips.txt$', gtfs_trips, name='gtfs-trips'),
    url(r'^stop_times.txt$', gtfs_stop_times, name='gtfs-stop_times'),

    # condicionalmente obligatorio
    url(r'^calendar.txt$', gtfs_calendar, name='gtfs-calendar'),
    #url(r'^calendar_dates.txt$', gtfs_calendar_dates, name='gtfs-calendar_dates'),

    #opcionales
    #url(r'^fare_rules.txt$', gtfs_fare_rules, name='gtfs-fare_rules'),
    url(r'^shapes.txt$', gtfs_shapes, name='gtfs-shapes'),
    url(r'^frequencies.txt$', gtfs_frequencies, name='gtfs-frequencies'),
    #url(r'^transfers.txt$', gtfs_transfers, name='gtfs-transfers'),
    url(r'^feed_info.txt$', gtfs_feed_info, name='gtfs-feed_info'),
]


"""
Archivos a generar según la especificacion
https://developers.google.com/transit/gtfs/reference/?hl=es-419

agency.txt	Obligatorio	Una o varias empresas de transporte público que proporcionan los datos de este feed.
stops.txt	Obligatorio	Ubicaciones específicas en las que los vehículos buscan o dejan pasajeros.
routes.txt	Obligatorio	Rutas de transporte público. Una ruta es un grupo de viajes que se muestra a los pasajeros como un solo servicio.
trips.txt	Obligatorio	Viajes para cada ruta. Un viaje es una secuencia de dos o más paradas que tienen lugar a una hora específica.
stop_times.txt	Obligatorio	Horarios a los que un vehículo llega a una parada específica y sale de ella en cada viaje.
calendar.txt	Condicionalmente obligatorio	Fechas de los ID de servicio en un horario semanal. Se especifica cuándo comienza y finaliza un servicio, así como los días de la semana en que el servicio está disponible. Este archivo es obligatorio a menos que todas las fechas de servicio se definan en el archivo calendar_dates.txt.
calendar_dates.txt	Condicionalmente obligatorio	Excepciones de los ID de servicio definidos en el archivo calendar.txt. Si se omite el archivo calendar.txt, entonces el archivo calendar_dates.txt es obligatorio y debe contener todas las fechas de servicio.
fare_rules.txt	Opcional	Reglas de aplicación de la información sobre tarifas correspondiente a las rutas de una organización de transporte público.
shapes.txt	Opcional	Reglas para el trazado de las líneas en un mapa que representen las rutas de una organización de transporte público.
frequencies.txt	Opcional	Tiempo entre viajes para las rutas cuya frecuencia de servicio es variable.
transfers.txt	Opcional	Reglas para establecer conexiones en los puntos de transbordo entre rutas.
feed_info.txt	Opcional	Información adicional sobre el feed en sí, incluida la información sobre el editor, la versión y el vencimiento.

"""

