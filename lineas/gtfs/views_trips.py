"""

Specs: https://developers.google.com/transit/gtfs/reference/?hl=es-419#stopstxt


"""
import django_excel as excel
from lineas.models import Recorrido
from colectivos.models import EstacionAnioFrecuencias, FranjaHorariaFrecuencias
from django.conf import settings
import os
from django.views.decorators.cache import cache_page
import shutil


def gen_trips_txt(dest_file='trips'):
    csv_list = []
    headers = ['route_id', 'service_id', 'trip_id', 'trip_headsign', 'trip_short_name',
                'direction_id', 'block_id', 'shape_id', 'wheelchair_accessible', 
                'bikes_allowed'
                ]
    csv_list.append(headers)

    # me pide
    recorridos = Recorrido.objects.filter(publicado=True, linea__publicado=True).exclude(descripcion_corta='').exclude(trazado__isnull=True)
    horarios = FranjaHorariaFrecuencias.objects.all()

    for recorrido in recorridos:
        for horario in horarios:
            #como id trip consideramos el recorrido + franja horaria
            trip = '{}_{}'.format(recorrido.id, horario.id)
            lst = [recorrido.linea.id,
                    horario.id, # cada servicio será una franja horaria diferente
                    trip,
                    recorrido.descripcion_interna,  # trip_headsign (cartel que muestra el bondi cuando hace este recorrido)
                    '', # trip_short_name
                    '',  # direction_id IDA=0, VUELTA=1 #TODO no tenemos esto en nuestra base
                    '',  # block_id #TODO si hubiera info de viaje en tramos este se usaría
                    recorrido.id,  # shape_id ID de los mapas. Los IDs son iguales a estos
                    0,  # wheelchair_accessible 0=no se
                    2  # se permite bicicletas? 2=NO
                    ]

            csv_list.append(lst)

    # guardar el archivo para comprimir y futuras referencias
    dest_fold = os.path.join(settings.MEDIA_ROOT, 'gtfs')
    if not os.path.exists(dest_fold):
        os.makedirs(dest_fold)
    dest_csv = os.path.join(dest_fold, dest_file + '.csv')
    sheet = excel.pe.Sheet(csv_list)
    sheet.save_as(filename=dest_csv)

    shutil.copyfile(dest_csv, dest_csv.replace('.csv', '.txt'))
    return  sheet

@cache_page(60 * 60 * 24)  # 1 h
def gtfs_trips(request):
    """
    routes.txt from GTFS spec
    """
    sheet = gen_trips_txt()
    return excel.make_response(sheet, 'csv')
