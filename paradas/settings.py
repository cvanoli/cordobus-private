URL_BASE_WS_PROVEEDOR = 'https://gobiernoabierto.cordoba.gob.ar/api/v2/transporte-publico/'

USER_WS_PROVEEDOR = ''
PASS_WS_PROVEEDOR = ''

# configuración oculta
try:
    from .local_settings import *
except ImportError:
    pass