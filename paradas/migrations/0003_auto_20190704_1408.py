# Generated by Django 2.2 on 2019-07-04 17:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paradas', '0002_auto_20181204_1940'),
    ]

    operations = [
        migrations.AddField(
            model_name='posteparada',
            name='id_efisat',
            field=models.CharField(blank=True, help_text='ID en el webservice de EFISAT', max_length=90, null=True),
        ),
        migrations.AlterField(
            model_name='parada',
            name='verificado',
            field=models.BooleanField(default=True, help_text='Marcar como falso si la parada dejó de existir.'),
        ),
        migrations.AlterField(
            model_name='posteparada',
            name='id_externo',
            field=models.CharField(blank=True, help_text='ID en el webservice de gobierno abierto córdoba', max_length=90, null=True),
        ),
    ]
