#!/usr/bin/python
"""
Importar paradas de transporte desde webservice del proveedor EFISAT asociadas a la bandera
"""
from django.utils.text import slugify
from django.core.management.base import BaseCommand
from lineas.models import Linea, Recorrido
from paradas import settings as settings_tp
from paradas.models import Parada, PosteParada
from django.db import transaction
import sys
import datetime
import requests
import json
import xml.etree.ElementTree as etree
from django.contrib.gis.geos import Point
from django.db.models import Q


class Command(BaseCommand):
    help = """Comando para Importar paradas de todas las lineas de transporte desde webservice del proveedor EFISAT"""

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando actualizacion de paradas de transporte'))
        # requiere un POST con user y PASS

        headers = {'Content-Type': 'application/soap+xml'}

        tot_nuevos_postes = 0
        tot_repetidos_postes = 0
        errores = []
        tot_nuevas_paradas = 0
        tot_repetidas_paradas = 0
        tot_actualizadas_paradas = 0

        lineas = Linea.objects.filter(publicado=True)
        for linea in lineas:
            self.stdout.write(self.style.SUCCESS(' ********* \n Buscando paradas línea {} {} {} \n ************ \n'.format(linea.nombre_publico, linea.empresa.nombre_publico, linea.id_externo)))

            body = """<?xml version="1.0" encoding="utf-8"?>
                      <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                        <soap12:Body>
                          <RecuperarParadasCompletoPorLinea xmlns="http://clsw.smartmovepro.net/">
                            <usuario>{user}</usuario>
                            <clave>{passw}</clave>
                            <codigoLineaParada>{linea_id_externo}</codigoLineaParada>
                          </RecuperarParadasCompletoPorLinea>
                        </soap12:Body>
                      </soap12:Envelope>""".format(user=settings_tp.USER_WS_PROVEEDOR,
                                                  passw=settings_tp.PASS_WS_PROVEEDOR,
                                                  linea_id_externo=linea.id_externo
                                                  )

            url = settings_tp.URL_BASE_WS_PROVEEDOR
            self.stdout.write(self.style.SUCCESS('POST TO {}'.format(url)))
            response = requests.post(url, data=body, headers=headers)

            # print(response.text)
            # sys.exit(1)
            """
            <?xml version="1.0" encoding="utf-8"?>
            <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
              <soap12:Body>
                <RecuperarParadasCompletoPorLineaResponse xmlns="http://clsw.smartmovepro.net/">
                  <RecuperarParadasCompletoPorLineaResult>string</RecuperarParadasCompletoPorLineaResult>
                </RecuperarParadasCompletoPorLineaResponse>
              </soap12:Body>
            </soap12:Envelope>
            """

            data = etree.fromstring(response.text)
            sopa = data[0]  # soap:Envelope
            recupera = sopa[0]  # RecuperarParadasCompletoPorLineaResponse
            results = recupera[0]  # <RecuperarParadasCompletoPorLineaResult>

            try:
                results_json = json.loads(results.text)
            except Exception as e:
                error_str = 'Error al cargar el json de la línea {}'.format(linea)
                self.stdout.write(self.style.ERROR('ERROR en datos del json: {}'.format(e)))
                errores.append(error_str)
                continue
            # print(results_json)
            # print(results_json['paradas'])
            # print(type(results_json['paradas']))
            """
            {'MensajeEstado': 'ok', 'CodigoEstado': 0, 'paradas':
                {'10 V': [{'Codigo': '31975', 'AbreviaturaBandera': '10 V', 'Descripcion': 'C1000', 'Identificador': 'C1000', 'AbreviaturaBanderaGIT': '10 V', 'LongitudParada': '-64,273549', 'AbreviaturaAmpliadaBandera': 'A ITUZAINGO', 'LatitudParada': '-31,344511'},
                          {'Codigo': '31976', 'AbreviaturaBandera': '10 V', 'Descripcion': 'C1002', 'Identificador': 'C1002', 'AbreviaturaBanderaGIT': '10 V', 'LongitudParada': '-64,273496', 'AbreviaturaAmpliadaBandera': 'A ITUZAINGO', 'LatitudParada': '-31,342087'}
                          .
                          .
                          .
                          ],
                'REG40': [{
                    'Codigo': '47263',
                    'Identificador': 'C7072',
                    'Descripcion': 'AL CENTRO',
                    'AbreviaturaBandera': 'REG40',
                    'AbreviaturaAmpliadaBandera': 'REG. A Bº 20 DE JUNIO',
                    'LatitudParada': '-31,357054',
                    'LongitudParada': '-64,156680',
                    'AbreviaturaBanderaGIT': 'REG40'
                            },
                    {
                    'Codigo': '47264',
                    'Identificador': 'C7073',
                    'Descripcion': 'AL CENTRO',
                    'AbreviaturaBandera': 'REG40',
                    'AbreviaturaAmpliadaBandera': 'REG. A Bº 20 DE JUNIO',
                    'LatitudParada': '-31,358855',
                    'LongitudParada': '-64,154390',
                    'AbreviaturaBanderaGIT': 'REG40'
                    },
                    .
                    .
                    .
                    ]
                }
            }
            """

            # self.stdout.write(self.style.SUCCESS('{} sopa'.format(sopa)))
            # self.stdout.write(self.style.SUCCESS('{} recupera'.format(recupera)))
            # self.stdout.write(self.style.SUCCESS('RES[0] = {}'.format(results_json)))

            # self.stdout.write(self.style.SUCCESS('{} RESULTADOS'.format(len(results_json))))

            # chequear los datos
            nuevos_postes = 0
            repetidos_postes = 0

            nuevas_paradas = 0
            repetidas_paradas = 0
            actualizadas_paradas = 0

            if results_json['MensajeEstado'] == 'ok':
                for k, v in results_json['paradas'].items():
                    # k es el recorrido de la/s linea/s sobre la que estoy iterando
                    # v es la lista de dict donde cada dict tiene la forma:

                    # {
                    # 'Codigo': '47264',
                    # 'Identificador': 'C7073',
                    # 'Descripcion': 'AL CENTRO',
                    # 'AbreviaturaBandera': 'REG40',
                    # 'AbreviaturaAmpliadaBandera': 'REG. A Bº 20 DE JUNIO',
                    # 'LatitudParada': '-31,358855',
                    # 'LongitudParada': '-64,154390',
                    # 'AbreviaturaBanderaGIT': 'REG40'
                    # }

                    try:
                        # solo si existe el recorrido importamos las paradas asociadas
                        recorrido = Recorrido.objects.get(descripcion_corta=slugify(k), publicado=True)
                        self.stdout.write(self.style.SUCCESS('Recorrido {}'.format(slugify(k))))
                        Parada.objects.filter(recorrido=recorrido).update(verificado=False)

                    except Exception as e:
                        #excepcion asociada a la falta de un recorrido
                        self.stdout.write(self.style.ERROR(e))
                        error_str = 'El recorrido {} de la linea {} no está registrado \n'.format(slugify(k) ,linea)
                        self.stdout.write(self.style.ERROR(error_str))
                        errores.append(error_str)
                        recorrido = None

                    if recorrido is not None:
                        if len(v) < 10:
                            error_str = 'El recorrido {} tiene menos de 10 paradas. Revisar'.format(k)
                            self.stdout.write(self.style.ERROR(error_str))
                            errores.append(error_str)

                        # despublicamos las paradas del recorrido
                        Parada.objects.filter(recorrido=recorrido).update(verificado=False)
                        for parada in v:
                            # self.stdout.write(self.style.SUCCESS('{} paradas'.format(len(v))))

                            #actualizo info del recorrido
                            recorrido.descripcion_larga = parada['AbreviaturaAmpliadaBandera']
                            recorrido.save()

                            #identifico el poste por su ubicacion, si no existe lo creo
                            latitud = float(parada['LatitudParada'].replace(',', '.'))
                            longitud = float(parada['LongitudParada'].replace(',', '.'))
                            ubicacion = Point(longitud, latitud)

                            try:
                                poste = PosteParada.objects.get(ubicacion=ubicacion)
                                repetidos_postes += 1
                            except Exception:
                                poste = PosteParada.objects.create(ubicacion=ubicacion,
                                            identificador=parada['Identificador'])
                                nuevos_postes += 1

                            poste.descripcion = parada['Descripcion']
                            poste.save()

                            # Por defecto se crea como verificado = True
                            try:
                                parada_obj = Parada.objects.get(Q(poste=poste,
                                                                codigo=parada['Identificador'],
                                                                recorrido=recorrido) |
                                                            Q(poste=poste,
                                                                recorrido=recorrido))
                                parada_obj.verificado = True

                                #El identificador ahora pertenece al poste
                                parada_obj.codigo = parada['Codigo']
                                repetidas_paradas += 1

                            except Exception:
                                parada_obj = Parada.objects.create(poste=poste,
                                            codigo=parada['Codigo'],
                                            recorrido=recorrido)
                                nuevas_paradas += 1
                                self.stdout.write(self.style.SUCCESS('Nueva parada {}'.format(parada_obj.codigo)))

                            parada_obj.save()

                        self.stdout.write(self.style.SUCCESS('********* OK ********* \n Postes: nuevos:{} - repetidos:{}  \n paradas nuevas:{} - repetidas:{}'.format(nuevos_postes,
                                                                                                                                                                    repetidos_postes,
                                                                                                                                                                    nuevas_paradas,
                                                                                                                                                                    repetidas_paradas)))
                        tot_nuevos_postes += nuevos_postes
                        tot_repetidos_postes += repetidos_postes
                        tot_nuevas_paradas += nuevas_paradas
                        tot_repetidas_paradas += repetidas_paradas
                        tot_actualizadas_paradas += actualizadas_paradas

            else:
                #Mensaje de estado != ok
                error_str = 'Mensaje de estado {} - Código {} - Linea {}'.format(results_json['MensajeEstado'], results_json['CodigoEstado'], linea)
                self.stdout.write(self.style.ERROR(' Error: {}'.format(error_str)))
                errores.append(error_str)

        if len(errores) > 0:
            self.stdout.write(self.style.ERROR('{} errores'.format(len(errores))))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))

        self.stdout.write(self.style.SUCCESS('********* FIN ********* \n Postes: nuevos:{} - repetidos:{}  \n paradas nuevas:{} - repetidas:{} - actualizadas:{}'.format(tot_nuevos_postes,
                                                                                                                                                        tot_repetidos_postes,
                                                                                                                                                        tot_nuevas_paradas,
                                                                                                                                                        tot_repetidas_paradas,
                                                                                                                                                        tot_actualizadas_paradas)))
