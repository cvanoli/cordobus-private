from django.utils.text import slugify
from django.core.management.base import BaseCommand
from paradas.models import PosteParada
from paradas import settings as settings_tp
from django.db import transaction
import sys
import datetime
import requests
import json
from django.contrib.gis.geos import Point


class Command(BaseCommand):
    help = """Comando para importar postes de paradas de todas las lineas de transporte desde webservice del proveedor"""

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación de paradas de transporte'))

        tot_nuevos_postes = 0
        tot_repetidos_postes = 0

        url = settings_tp.URL_BASE_WS_PROVEEDOR+'postes-paradas-colectivos/'
        self.stdout.write(self.style.SUCCESS('Extrayendo datos de {}'.format(url)))

        while True:

            try:
                response = requests.get(url)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERROR trayendo datos: {} \n'.format(e)))
                sys.exit(1)

            json_data = json.loads(response.text)
            results = json_data['results']
            url = json_data['next']

            for feature in results['features']:
                propiedades = feature['properties']

                poste, created = PosteParada.objects.get_or_create(id_externo=feature['id'])

                poste.descripcion = propiedades['descripcion']
                poste.ubicacion = Point(feature['geometry']['coordinates'])
                poste.id_efisat = propiedades['id_externo']
                poste.save()

                if not created:
                    self.stdout.write(self.style.SUCCESS('Poste existente {}'.format(poste.id_externo)))
                    tot_repetidos_postes += 1
                else:
                    self.stdout.write(self.style.SUCCESS('Nuevo poste {}'.format(poste.id_externo)))
                    tot_nuevos_postes += 1

            if url is None:
                break

        self.stdout.write(self.style.SUCCESS('********* FIN ********* \n Postes: nuevos:{} - repetidos:{}'.format(
            tot_nuevos_postes, tot_repetidos_postes)))
