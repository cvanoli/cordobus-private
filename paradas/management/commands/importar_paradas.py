from django.utils.text import slugify
from django.core.management.base import BaseCommand
from paradas.models import Parada, PosteParada
from paradas import settings as settings_tp
from lineas.models import Recorrido
from django.db import transaction
import sys
import datetime
import requests
import json
from django.contrib.gis.geos import Point
from django.db.models import Q


class Command(BaseCommand):
    help = """Comando para importar paradas de todas las lineas de transporte desde webservice del proveedor"""

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación de paradas de transporte'))

        errores = []
        nuevas_paradas = 0
        repetidas_paradas = 0

        url = settings_tp.URL_BASE_WS_PROVEEDOR+'postes-con-paradas'
        self.stdout.write(self.style.SUCCESS('Extrayendo datos de {}'.format(url)))

        while True:

            try:
                response = requests.get(url)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERROR trayendo datos: {} \n'.format(e)))
                sys.exit(1)

            json_data = json.loads(response.text)
            results = json_data['results']
            url = json_data['next']

            # chequear los datos

            for feature in results:
                poste_id_externo = feature['id']

                for parada_dict in feature['paradas']:

                    # es un dict
                    linea_dict = parada_dict['linea']

                    #es un dict
                    recorrido_dict = parada_dict['recorrido']

                    try:
                        poste = PosteParada.objects.get(id_externo=poste_id_externo)
                    except Exception as e:
                        #excepcion asociada a la falta de un poste inexistente en el sistema
                        self.stdout.write(self.style.ERROR(e))
                        error_str = 'El poste {} no está'
                        'registrado'.format(poste_id_externo)
                        self.stdout.write(self.style.ERROR(error_str))
                        errores.append(error_str)
                        continue

                    try:
                        recorrido = Recorrido.objects.get(#Q(id_externo=recorrido_dict['id_externo'], publicado=True) |
                                                            Q(descripcion_corta=recorrido_dict['descripcion_corta'], publicado=True))
                        self.stdout.write(self.style.SUCCESS('Recorrido {}'.format(recorrido)))

                    except Exception as e:
                        #excepcion asociada a la falta de un recorrido
                        self.stdout.write(self.style.ERROR(e))
                        error_str = 'El recorrido {}(id externo) de la linea {} no está registrado.'.format(recorrido_dict['id'], linea_dict['nombre_publico'])
                        self.stdout.write(self.style.ERROR(error_str))
                        errores.append(error_str)
                        continue

                    # si todo anduvo bien, creamos la parada
                    parada, created = Parada.objects.get_or_create(poste=poste, recorrido=recorrido)

                    if not created:
                        repetidas_paradas += 1

                    else:
                        nuevas_paradas += 1
                        parada.codigo = parada_dict['codigo']
                        parada.save()
                        self.stdout.write(self.style.SUCCESS('Nueva parada {}'.format(parada.id)))

            self.stdout.write(self.style.SUCCESS('********* OK *********'))

            if url is None:
                break

        if len(errores) > 0:
            self.stdout.write(self.style.ERROR('{} errores'.format(len(errores))))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))

        self.stdout.write(self.style.SUCCESS('********* FIN ********* \n Paradas nuevas:{} - repetidas:{}'.format(nuevas_paradas, repetidas_paradas)))
