from rest_framework import viewsets
from rest_framework.permissions import (IsAuthenticatedOrReadOnly,
                                        IsAuthenticated,
                                        DjangoModelPermissions,
                                        DjangoModelPermissionsOrAnonReadOnly)
from rest_framework.filters import OrderingFilter
from .serializers import *
from django.db.models import Q, F
from datetime import datetime, timedelta
from paradas.models import PosteParada, Parada
# from rest_framework_gis.filters import InBBoxFilter
from rest_framework.decorators import action
from core.pagination import DefaultPagination


class PosteParadaGeoViewSet(viewsets.ModelViewSet):
    """
    Postes y sus correspondientes paradas del transporte público de toda la ciudad
    con datos geográficos.
    Se puede filtrar con el parametro recorrido_id. En API/?recorrido_id=16

    Se puede usar bounding box con el parámetro 'in_bbox'. Formato de
    parámetros (min Lon, min Lat, max Lon, max Lat). Ejemplo:
    "in_bbox"=-64.19215920250672,-31.413600429103756,-64.19254544060477,-31.41472664471516

    """
    serializer_class = PosteParadaGeoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    bbox_filter_field = 'ubicacion'
    # filter_backends = (InBBoxFilter, )

    def get_queryset(self):
        queryset = PosteParada.objects.filter(paradas__verificado=True).distinct()

        line = self.request.query_params.get('linea', None)
        if line is not None:
            line = line.split(',')
            queryset = queryset.filter(paradas__recorrido__linea__nombre_publico__in=line, paradas__recorrido__linea__publicado=True)

        id_recorrido = self.request.query_params.get('recorrido_id', None)
        if id_recorrido is not None:
            id_recorrido = id_recorrido.split(',')
            queryset = queryset.filter(paradas__recorrido__id__in=id_recorrido)
        
        poste_id = self.request.query_params.get('poste_id', None)
        if poste_id is not None:
            poste_id = poste_id.split(',')
            queryset = queryset.filter(id__in=poste_id)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ParadaViewSet(viewsets.ModelViewSet):
    """
    Paradas del transporte público de toda la ciudad georefenciadas.

    Se puede filtrar por linea con el parámetro 'linea'. Por ejemplo:
    "linea=41". Pueden ser varias lineas separadas por comas.
    Se puede filtrar por empresa con el parámetro 'e'. Por ejemplo:
    "e=CONIFERAL".
    Se puede filtrar por id del poste con el parámetro 'poste_id'. Por ejemplo:
    "poste_id=1". Pueden ser varios ids separados por comas. Para consultar los postes,
    ir a https://cordobus.apps.cordoba.gob.ar/paradas-colectivos/api/postes-paradas-colectivos/
    Se puede filtrar por id del recorrido con el parámetro 'recorrido_id'. Por ejemplo:
    "recorrido_id=1". Pueden ser varios ids separados por comas. Para consultar los
    recorridos, ir a https://cordobus.apps.cordoba.gob.ar/lineas/api/recorrido/
    """
    serializer_class = ParadaGeoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Parada.objects.filter(verificado=True)

        line = self.request.query_params.get('linea', None)
        if line is not None:
            line = line.split(',')
            queryset = queryset.filter(recorrido__linea__nombre_publico__in=line, recorrido__linea__publicado=True)

        e = self.request.query_params.get('e', None)
        if e is not None:
            queryset = queryset.filter(recorrido__linea__empresa__nombre_publico__icontains=e)

        poste_id = self.request.query_params.get('poste_id', None)
        if poste_id is not None:
            poste_id = poste_id.split(',')
            queryset = queryset.filter(poste__id__in=poste_id)

        recorrido_id = self.request.query_params.get('recorrido_id', None)
        if recorrido_id:
            recorrido_id = recorrido_id.split(',')
            queryset = queryset.filter(recorrido__id__in=recorrido_id)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)