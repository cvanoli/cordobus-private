from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer, GeometrySerializerMethodField
from paradas.models import *
from lineas.api.serializers import LineaSerializer, RecorridoSinGeoSerializer


class ParadaSerializer(CachedSerializerMixin):

    recorrido = serializers.SerializerMethodField()

    def get_recorrido(self, obj):
        return obj.recorrido.descripcion_corta

    class Meta:
        model = Parada
        fields = ['id', 'codigo', 'recorrido']


class PosteParadaGeoSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):
    """
    cada una de las garitas o postes donde puede haber una o más paradas
    """
    paradas = ParadaSerializer(read_only=True, many=True)

    class Meta:
        model = PosteParada
        geo_field = 'ubicacion'
        auto_bbox = True
        fields = ['id', 'identificador', 'descripcion', 'paradas']


class PosteParadaSerializer(CachedSerializerMixin):
    """
    cada una de las garitas o postes donde puede haber una o más paradas
    """
    class Meta:
        model = PosteParada
        fields = ['id', 'identificador', 'descripcion']


class ParadaGeoSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):
    poste = PosteParadaSerializer(read_only=True)
    recorrido = RecorridoSinGeoSerializer(read_only=True)
    ubicacion = GeometrySerializerMethodField()

    def get_ubicacion(self, obj):
        return obj.poste.ubicacion

    class Meta:
        model = Parada
        geo_field = 'ubicacion'
        fields = ['id', 'codigo', 'recorrido', 'poste']


# Registro los serializadores en la cache de DRF
cache_registry.register(PosteParadaSerializer)
cache_registry.register(ParadaGeoSerializer)
cache_registry.register(ParadaSerializer)
cache_registry.register(PosteParadaGeoSerializer)
