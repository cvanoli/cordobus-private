$( document ).ready(function() {

    $("#btnViajes").click(function () {

        var recorrido_seleccionado = $("#recorrido :selected").val();

        if (recorrido_seleccionado) {
            const url = '/lineas/api/recorrido/'
            cargar_recorrido(url + recorrido_seleccionado, recorrido_seleccionado);
        }

    });
});