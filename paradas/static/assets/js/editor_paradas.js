function styleFunction(feature, resolution) {
    linea = feature.get('linea');

    var styles = [
        new ol.style.Style({
            stroke: new ol.style.Stroke({
              color: linea['color'],
              width: 2
            }),
        }),
    ];

    return styles;
}

function styleParada(feature, resolution) {
    linea = feature.get('linea');

    var styles = [
        new ol.style.Style({
            stroke: new ol.style.Stroke({
              color: linea['color'],
              width: 2
            }),
        }),
    ];

    return styles;
}

function icon_style(color)
{
    var style = new ol.style.Style({
        image: new ol.style.Icon(/** @type {module:ol/style/Icon~Options} */ ({
            color: color,
            crossOrigin: 'anonymous',
            src: 'https://openlayers.org/en/latest/examples/data/dot.png'
          }))
    });

    return style;
}

var map = new ol.Map({
    layers: [
        new ol.layer.Tile({
            source: new ol.source.OSM({
                    'url': 'http://{a-c}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png})'
                    }),
               // opacity: 0.4,
        })
    ],
    target: document.getElementById('map'),
    view: new ol.View({
        center: ol.proj.fromLonLat([-64.1862, -31.4074]),
        zoom: 12.5
    })
});

function cargar_recorrido(next, recorrido) {
    const id_recorrido = recorrido
    fetch(next).then(function(response) {
        return response.json();
    }).then(function(json) {

        if(json != undefined)
        {
            var vectorSource = new ol.source.Vector({
                features: (new ol.format.GeoJSON()).readFeatures(json, {
                    featureProjection:"EPSG:3857"
                })
            });

            var vectorLayer = new ol.layer.Vector({
                source: vectorSource,
                style: styleFunction
            });

            map.addLayer(vectorLayer);
        }

        if (json.next != null)
        {
            cargar_recorrido(json.next)
        }
        /*se cargan las paradas*/
        const url = '/paradas-colectivos/api/paradas-colectivos/'
        cargar_postes_y_paradas(url + '?recorrido_id=' + id_recorrido);
    });
}

function cargar_postes_y_paradas(next) {
    fetch(next.replace('http', 'https')).then(function(response) {
        return response.json();
    }).then(function(json) {
        if(json.results != undefined)
        {
            var vectorSource = new ol.source.Vector({
                features: (new ol.format.GeoJSON()).readFeatures(json.results, {
                    featureProjection:"EPSG:3857"
                })
            });

            var vectorLayer = new ol.layer.Vector({
                source: vectorSource,
                style: icon_style('blue')
            });

            map.addLayer(vectorLayer);
        }

        if (json.next != null)
        {
            cargar_postes_y_paradas(json.next)
        }
    });
}

function limpiar_mapa() {

    while (vector_exists()){
        map.getLayers().forEach(function (layer) {
            if (layer != undefined) {
/*                console.log('layer.getType(): ', layer.getType());*/

                if (layer.getType() === 'VECTOR') {
/*                    console.log('layer deleted.');*/

                    map.removeLayer(layer);
                }
            }
        });
    }
}

function vector_exists() {
    const layers = map.getLayers().getArray();
    for (let index = 0; index < layers.length; index++)
    {
        const layer = layers[index];
        if (layer.getType() === 'VECTOR')
        {
            return true;
        }
    }

    return false;
}

var element = document.getElementById('popup');

var popup = new ol.Overlay.Popup({
    element: element,
    positioning: 'bottom-center',
    stopEvent: false,
    offset: [0, -8]
});
map.addOverlay(popup);

// display popup on click
map.on('click', function(evt) {
/*    console.log("onclick: ", evt.pixel);*/

    /* Detecta el feature en el mapa */
    var feature = map.forEachFeatureAtPixel(evt.pixel,
        function(feature) {
            return feature;
        });

    if (feature){
        var coordinates = evt.coordinate;

        var id_parada = feature.getId();
        var identificador_parada = feature.get('codigo');
        var poste = feature.get('poste');
        var descripcion = poste['descripcion'];
        var identificador_poste = poste['identificador']
        var id_poste = poste['id']

        popup.show(coordinates,
            "<strong>" + "Parada: " + identificador_parada + "<br>" +
            "Descripción: " + descripcion + "<br>" +
            "Poste: " + identificador_poste + '<br>' +
            "Modificar parada: " + '<a href=https://cordobus.apps.cordoba.gob.ar/admin/paradas/parada/'+ id_parada +'/change/> Link al admin </a><br>' +
            "Modificar poste: " + '<a href=https://cordobus.apps.cordoba.gob.ar/admin/paradas/posteparada/'+ id_poste + '/change/> Link al admin </a><br>' +
            "</strong>");
    }
    else{
        popup.hide();
    }
});

// change mouse cursor when over marker
map.on('pointermove', function(e) {
    if (e.dragging) {
        popup.hide();
        return;
    }
    var pixel = map.getEventPixel(e.originalEvent);
    var hit = map.hasFeatureAtPixel(pixel);

    map.getTarget().style.cursor = hit ? 'pointer' : '';
});
