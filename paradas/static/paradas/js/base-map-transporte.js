
var map;
var div_map = document.getElementById('map');

function initMap() {
    map = new google.maps.Map(div_map, 
                                {center: 
                                {lat: -31.4177131, lng: -64.1956264},
                                zoom: 12,
                                mapTypeId: google.maps.MapTypeId.TERRAIN  // ROADMAP HYBRID
                                });
    var styleControl = document.getElementById('style-selector-control');
    map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(styleControl);

    map.data.addListener('click', function(event){
        let f = event.feature.f;
        if (f.tipo == 'poste') {
            clickPoste(f);
        }
        
    });

}


function getRandomColor() {
    // definirle un color
    let r1 = Math.floor((Math.random() * 9) + 6);
    let r2 = Math.floor((Math.random() * 9) + 6);
    let g1 = Math.floor((Math.random() * 9) + 6);
    let g2 = Math.floor((Math.random() * 9) + 6);
    let b1 = Math.floor((Math.random() * 9) + 6);
    let b2 = Math.floor((Math.random() * 9) + 6);
    // 15.toString(16) = F
    let color = r1.toString(16) + r2.toString(16) + g1.toString(16) + g2.toString(16) + b1.toString(16) + b2.toString(16);
    return color;
}