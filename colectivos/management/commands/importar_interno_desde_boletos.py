#!/usr/bin/python
from colectivos.models import Interno
from lineas.models import Empresa
from django.core.management.base import BaseCommand
from django.db import transaction
import pandas as pd
import sys



class Command(BaseCommand):
    help = """ Comando para importar internos 'perdidos' desde los cortes de boletos """

    def add_arguments(self, parser):
        parser.add_argument('--path', help='Ruta al CSV con los datos a importar.', default='/extrafs/corte_de_boleto_worldline/Pasajes_georeferenciados_20190322.csv')

    @transaction.atomic
    def handle(self, *args, **options):

        path = options['path']
        if path is None:
            self.stdout.write(self.style.ERROR('Debe especificar la ruta al archivo csv.'))
            sys.exit(1)

        self.stdout.write(self.style.SUCCESS('Importando csv desde {}'.format(path)))

        # Lista para registrar los internos que cabiaron de empresa
        internos_nuevos = []

        # DataFrame principal del csv pasado como argumento
        df = pd.read_csv(path, encoding="ISO-8859-1", sep=";", parse_dates=True)

        # Creamos otro df para manejar las columnas de empresas e internos
        internos = pd.DataFrame()
        internos['EMPRESA'] = df['EMPRESA']
        internos['INTERNO'] = df['COCHE']
        # Eliminamos internos duplicados
        internos_unicos = internos.drop_duplicates()
        # Convertimos el df a lista de lista.
        # Ej: [['Coniferal S.A.C.I.F.', 146], ['Coniferal S.A.C.I.F.', 206], ['Coniferal S.A.C.I.F.', 58], ...]
        lista_internos = internos_unicos.to_numpy().tolist()

        for lista in lista_internos:
            # No me pasan id, asi que tengo que tomar los strings
            empresas = lista[0]
            interno = str(lista[1])
            if empresas == "Coniferal S.A.C.I.F.":
                empresa = Empresa.objects.get(nombre_publico="Coniferal")
            elif empresas == "ERSA Urbano S.A":
                empresa = Empresa.objects.get(nombre_publico="ERSA")
            elif empresas == "Autobuses Córdoba S.R.L":
                empresa = Empresa.objects.get(nombre_publico="Autobuses Córdoba")
            elif empresas == "T.A.M. S.E. Trolebuses":
                empresa = Empresa.objects.get(nombre_publico="TAMSE")
            else:
                self.stdout.write(self.style.ERROR('Empresa no reconocida {}'.format(empresas)))
                sys.exit(1)

            interno_obj, created = Interno.objects.get_or_create(numero=interno, empresa=empresa)
            if created:
                interno_obj.observaciones = "Interno cargado desde corte de boletos"
                interno_obj.save()
                internos_nuevos.append(interno_obj.numero)


        self.stdout.write(self.style.SUCCESS('Internos nuevos registrados {}: {}'.format(len(internos_nuevos), internos_nuevos)))
        self.stdout.write(self.style.SUCCESS('Fin del script.'))
