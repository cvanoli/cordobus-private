from django.core.management.base import BaseCommand
import requests
import json
from django.db import transaction
from colectivos.models import Frecuencia, EstacionAnioFrecuencias, AgrupadorFranjaHoraria, DiasGrupoFrecuencias
from lineas.models import Linea


class Command(BaseCommand):
    help = """Comando para sincronizar las frecuencias con el webservice de Qhapax"""

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando sincronización..'))

        url_api_frecuencias = 'https://gobiernoabierto.cordoba.gob.ar/api/v2/transporte-publico/frecuencias/'
        nodos = []
        c = 1
        while url_api_frecuencias:
            # requests a la api de frecuencia en qhapax
            self.stdout.write(self.style.SUCCESS('Llamando API {} {}'.format(c, url_api_frecuencias)))
            c += 1
            try:
                r = requests.get(url_api_frecuencias, timeout=30, allow_redirects=True)
            except Exception as e:
                error = 'ERROR Llamando API {}'.format(e)
                self.stdout.write(self.style.ERROR(error))
                raise e

            r_json = r.json()
            self.stdout.write(self.style.SUCCESS('Resultados API {}'.format(r_json['count'])))
            # concatenamos todas las paginas en una sola
            nodos += r_json['results']
            url_api_frecuencias = r_json['next']

        # contador para ver cuantas frecuencias sincronizamos
        count = 0
        estacion = EstacionAnioFrecuencias.objects.get(nombre="Normal", anio=2018)
        habiles = DiasGrupoFrecuencias.objects.get(nombre="Habiles")
        sabados = DiasGrupoFrecuencias.objects.get(nombre="Sabados")
        domingos = DiasGrupoFrecuencias.objects.get(nombre="Domingos y Feriados")
        agrupadores = AgrupadorFranjaHoraria.objects.all()
        lineas = Linea.objects.all()
        for nodo in nodos:
            if (nodo['dias'] == "Habiles") and (nodo['agrupador'] == "Horario Pico"):
                frecuencia, created = Frecuencia.objects.get_or_create(estacion=estacion,
                                                                       agrupador=agrupadores.get(nombre="Horario Pico", dias=habiles),
                                                                       linea=lineas.get(nombre_publico=nodo['linea']),
                                                                       frecuencia_en_minutos_esperada=nodo['frecuencia_en_minutos_esperada']
                                                                      )
            elif (nodo['dias'] == "Sabados") and (nodo['agrupador'] == "Horario Pico"):
                frecuencia, created = Frecuencia.objects.get_or_create(estacion=estacion,
                                                                       agrupador=agrupadores.get(nombre="Horario Pico", dias=sabados),
                                                                       linea=lineas.get(nombre_publico=nodo['linea']),
                                                                       frecuencia_en_minutos_esperada=nodo['frecuencia_en_minutos_esperada']
                                                                      )
            elif (nodo['dias'] == "Domingos y Feriados") and (nodo['agrupador'] == "Horario Fijo"):
                frecuencia, created = Frecuencia.objects.get_or_create(estacion=estacion,
                                                                       agrupador=agrupadores.get(nombre="Horario Fijo", dias=domingos),
                                                                       linea=lineas.get(nombre_publico=nodo['linea']),
                                                                       frecuencia_en_minutos_esperada=nodo['frecuencia_en_minutos_esperada']
                                                                      )
            elif (nodo['dias'] == "Sabados") and (nodo['agrupador'] == "Horario Fijo"):
                frecuencia, created = Frecuencia.objects.get_or_create(estacion=estacion,
                                                                       agrupador=agrupadores.get(nombre="Horario Fijo", dias=sabados),
                                                                       linea=lineas.get(nombre_publico=nodo['linea']),
                                                                       frecuencia_en_minutos_esperada=nodo['frecuencia_en_minutos_esperada']
                                                                       )
            elif (nodo['dias'] == "Habiles") and (nodo['agrupador'] == "Horario Fijo"):
                frecuencia, created = Frecuencia.objects.get_or_create(estacion=estacion,
                                                                       agrupador=agrupadores.get(nombre="Horario Fijo", dias=habiles),
                                                                       linea=lineas.get(nombre_publico=nodo['linea']),
                                                                       frecuencia_en_minutos_esperada=nodo['frecuencia_en_minutos_esperada']
                                                                      )
            elif (nodo['dias'] == "Domingos y Feriados") and (nodo['agrupador'] == "Horario Nocturno"):
                frecuencia, created = Frecuencia.objects.get_or_create(estacion=estacion,
                                                                      agrupador=agrupadores.get(nombre="Horario Nocturno", dias=domingos),
                                                                      linea=lineas.get(nombre_publico=nodo['linea']),
                                                                      frecuencia_en_minutos_esperada=nodo['frecuencia_en_minutos_esperada']
                                                                      )
            elif (nodo['dias'] == "Sabados") and (nodo['agrupador'] == "Horario Nocturno"):
                frecuencia, created = Frecuencia.objects.get_or_create(estacion=estacion,
                                                                       agrupador=agrupadores.get(nombre="Horario Nocturno", dias=sabados),
                                                                       linea=lineas.get(nombre_publico=nodo['linea']),
                                                                       frecuencia_en_minutos_esperada=nodo['frecuencia_en_minutos_esperada']
                                                                      )
            elif (nodo['dias'] == "Habiles") and (nodo['agrupador'] == "Horario Nocturno"):
                frecuencia, created = Frecuencia.objects.get_or_create(estacion=estacion,
                                                                      agrupador=agrupadores.get(nombre="Horario Nocturno", dias=habiles),
                                                                      linea=lineas.get(nombre_publico=nodo['linea']),
                                                                      frecuencia_en_minutos_esperada=nodo['frecuencia_en_minutos_esperada']
                                                                      )
            elif (nodo['dias'] == "Domingos y Feriados") and (nodo['agrupador'] == "Horario Restante"):
                frecuencia, created = Frecuencia.objects.get_or_create(estacion=estacion,
                                                                       agrupador=agrupadores.get(nombre="Horario Restante", dias=domingos),
                                                                       linea=lineas.get(nombre_publico=nodo['linea']),
                                                                       frecuencia_en_minutos_esperada=nodo['frecuencia_en_minutos_esperada']
                                                                      )
            elif (nodo['dias'] == "Sabados") and (nodo['agrupador'] == "Horario Restante"):
                frecuencia, created = Frecuencia.objects.get_or_create(estacion=estacion,
                                                                       agrupador=agrupadores.get(nombre="Horario Restante", dias=sabados),
                                                                       linea=lineas.get(nombre_publico=nodo['linea']),
                                                                       frecuencia_en_minutos_esperada=nodo['frecuencia_en_minutos_esperada']
                                                                      )
            elif (nodo['dias'] == "Habiles") and (nodo['agrupador'] == "Horario Restante"):
                frecuencia, created = Frecuencia.objects.get_or_create(estacion=estacion,
                                                                       agrupador=agrupadores.get(nombre="Horario Restante", dias=habiles),
                                                                       linea=lineas.get(nombre_publico=nodo['linea']),
                                                                       frecuencia_en_minutos_esperada=nodo['frecuencia_en_minutos_esperada']
                                                                      )

        self.stdout.write(self.style.SUCCESS('FIN'))
