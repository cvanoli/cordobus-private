from django.apps import AppConfig


class ColectivosConfig(AppConfig):
    name = 'colectivos'
