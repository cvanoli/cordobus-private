from django.contrib.gis.db import models
from django.utils.text import slugify
from lineas.models import Linea



class MarcaVehiculo(models.Model):
    """
    Marcas de autos y otros vehiculos
    """
    nombre = models.CharField(max_length=90, unique=True)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']
        verbose_name = 'Marca de vehículo'
        verbose_name_plural = 'Marcas de vehiculos'


class ModeloVehiculo(models.Model):
    """
    Modelos de autos y otros vehiculos
    """
    marca = models.ForeignKey(MarcaVehiculo, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return '{} {}'.format(self.marca.nombre, self.nombre)

    class Meta:
        unique_together = (("nombre", "marca"),)
        ordering = ['marca__nombre', 'nombre']
        verbose_name = 'Modelo de vehículo'
        verbose_name_plural = 'Modelos de vehículos'


class TipoCombustible(models.Model):
    """ Tipo de combustible del vehículo """
    nombre = models.CharField(max_length=45, unique=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Tipo de combustible'
        verbose_name_plural = 'Tipos de combustibles'


class TipoAdaptado(models.Model):
    """
    Clase de adaptacion para personas con movilidad reducida u otras.
    Los valores pueden ser 'Piso bajo' o 'Adaptado' en el caso de Córdoba
    """
    nombre = models.CharField(max_length=45, unique=True)

    def __str__(self):
        return self.nombre


class OrigenDatoInterno(models.Model):
    '''
    Por ahora tenemos internos oficiales (desde la muni) y
    tambien tenemos internos que vienen desde Worldline (por cortes de boletos)
    '''
    nombre = models.CharField(max_length=190)

    def __str__(self):
        return self.nombre


class Interno(models.Model):
    """ unidades de colectivo """

    numero = models.CharField(
        max_length=20, help_text=u'Numero único (no repetible en el futuro) de interno dentro\
         del sistema'
    )
    dominio = models.CharField(max_length=15, help_text='Chapa patente')
    activo = models.BooleanField(
        default=True, help_text='El coche esta en período de actividad'
    )
    modelo = models.ForeignKey(ModeloVehiculo, on_delete=models.SET_NULL,
                               null=True, blank=True,
                               related_name='colectivos')
    empresa = models.ForeignKey('lineas.Empresa', on_delete=models.SET_NULL, null=True,
                                blank=True, related_name='unidades')
    chasis_marca = models.CharField(max_length=60, null=True, blank=True)
    chasis_numero = models.CharField(max_length=60, null=True, blank=True,
                                     unique=True)
    motor_numero = models.CharField(max_length=60, null=True, blank=True,
                                    unique=True)
    adaptado = models.ManyToManyField(TipoAdaptado, blank=True)
    modelo_anio = models.PositiveIntegerField(null=True, blank=True)
    tanque_litros = models.PositiveIntegerField(null=True, blank=True, help_text='Capacidad máxima del tanque de combustible')

    combustible = models.ForeignKey(
        TipoCombustible, null=True, blank=True, on_delete=models.SET_NULL
    )
    asientos = models.PositiveIntegerField(null=True, blank=True, help_text='Cantidad de asientos')
    fecha_alta = models.DateField(null=True, blank=True)
    fecha_baja = models.DateField(null=True, blank=True)
    expediente = models.CharField(max_length=60, null=True, blank=True)
    resolucion = models.CharField(max_length=60, null=True, blank=True)
    origen = models.ForeignKey(OrigenDatoInterno, on_delete=models.SET_NULL, null=True, blank=True)

    observaciones = models.TextField(null=True, blank=True)
    hay_que_revisar = models.BooleanField(default=False, help_text='Marcar este registro como con datos a revisar')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} int {}'.format(self.dominio, self.numero)

    def limpiar_dominio(self):
        return slugify(self.dominio).replace('-', '')

    def limpiar_numero(self):
        return slugify(self.numero).replace('-', '')

    def save(self, *args, **kwargs):
        """
        Se sobreescribe para que aplique slug al agregar o cambiar un interno
        """
        self.dominio = self.limpiar_dominio()
        self.numero = self.limpiar_numero()
        super(Interno, self).save(*args, **kwargs)

    class Meta:
        unique_together = ('numero', 'empresa',)


#
# Modelos referidos a frecuencia
#


class EstacionAnioFrecuencias(models.Model):
    """
    Cada una de las estaciones del año donde son validas las
    frecuencias exigidas
    """
    anio = models.IntegerField()
    nombre = models.CharField(max_length=30)
    activa = models.BooleanField(default=False)

    def __str__(self):
        return '{} {}'.format(self.nombre, self.anio)

    class Meta:
        verbose_name_plural = 'Estación del año para frecuencia'


class DiasGrupoFrecuencias(models.Model):
    """ Agrupación de días para computas las frecuencuas
        En general sera LaV, Sabados, Domingos y Feriados """
    nombre = models.CharField(max_length=30)

    lunes = models.BooleanField(default=False)
    martes = models.BooleanField(default=False)
    miercoles = models.BooleanField(default=False)
    jueves = models.BooleanField(default=False)
    viernes = models.BooleanField(default=False)
    sabado = models.BooleanField(default=False)
    domingo = models.BooleanField(default=False)
    feriado = models.BooleanField(default=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Grupo de días para frecuencias'


class FranjaHorariaFrecuencias(models.Model):
    """ Agrupación de horas con diferentes frecuencias """
    agrupador = models.ForeignKey('AgrupadorFranjaHoraria', null=True, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=30)
    hora_desde = models.TimeField()
    hora_hasta = models.TimeField()

    def __str__(self):
        return '{}: {} - {}'.format(self.nombre, self.hora_desde,
                                    self.hora_hasta)

    class Meta:
        verbose_name_plural = 'Franja Horaria de Frecuencias'


class AgrupadorFranjaHoraria(models.Model):
    ''' Las franjas se agrupan en Horarios y se relacionan con los dias '''
    nombre = models.CharField(max_length=30)
    dias = models.ForeignKey(DiasGrupoFrecuencias, on_delete=models.CASCADE)

    def __str__(self):
        return '{} - {}'.format(self.nombre, self.dias.nombre)


class Frecuencia(models.Model):
    estacion = models.ForeignKey(EstacionAnioFrecuencias, null=True, on_delete=models.CASCADE)
    agrupador = models.ForeignKey(AgrupadorFranjaHoraria, null=True, on_delete=models.CASCADE)
    linea = models.ForeignKey(Linea, null=True, on_delete=models.CASCADE)
    frecuencia_en_minutos_esperada = models.PositiveIntegerField(default=0)
    descripcion = models.TextField(null=True, blank=True)

    def get_full_data(self):
        return 'Línea: {}, {} minutos, {} unidades.\nEstacion: {}\nAgrupador:{} \n{}'.format(self.linea, self.frecuencia_en_minutos_esperada, self.unidades_activas_esperadas, self.estacion, self.agrupador, self.descripcion)

    def __str__(self):
        return '{} {} {} {}'.format(self.linea, self.frecuencia_en_minutos_esperada, self.agrupador, self.estacion) 

    def calculo_unidades_activas_esperadas(self):
        if self.linea.nombre_publico == 'AEROBUS':
            unidades_activas_esperadas = round(self.linea.tiempo_de_recorrido/30, 0)
        else:
            if self.frecuencia_en_minutos_esperada == 0:
                unidades_activas_esperadas = 0
            else:
                # Este es el calculo que usan para unidades activas esperadas
                unidades_activas_esperadas = round(self.linea.tiempo_de_recorrido/self.frecuencia_en_minutos_esperada, 0)
        return int(unidades_activas_esperadas)

    def save(self, *args, **kwargs):
        self.calculo_unidades_activas_esperadas()
        super(Frecuencia, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Frecuencias'
        verbose_name = 'Frecuencia'
        unique_together = (("estacion", "agrupador", "linea"))
