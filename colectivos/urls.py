from django.conf.urls import url
from colectivos.views import ListaInternos


urlpatterns = [
    url(r'^lista-de-internos.(?P<filetype>csv|xls)$', ListaInternos, name='colectivos.internos'),
]
