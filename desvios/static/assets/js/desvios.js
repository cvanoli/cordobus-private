$( document ).ready(function() {

    /***
    * se cargan todas las trayectorias cuando se renueva la pagina y no en cada peticion,
    * luego segun el recorrido se modifica el layer de trayectorias con la necesitada si es que esta
    ***/
    var trayectorias;  // recorridos de todos las lineas de transporte
    leerTrayectorias();  // se cargan vía API
    var currentViaje = null;

    var loading = $('#loading');


    $('#fechaFormField').datepicker({
        minDate: fechaDesde,
        maxDate: fechaHasta,
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
    });

    var handle = $( "#custom-handle" );
    var $umbralSlider = $( "#umbral-slider" );

    $umbralSlider.slider({
      range: "min",
      value: 100,
      min: 1,
      max: 1000,
      disabled:true,
      create: function() {
        handle.text( $( this ).slider( "value" ) );
      },
      slide: function( event, ui ) {
        //umbral = ui.value;
        setUmbral(ui.value);
        handle.text( ui.value );
        //$( "#amount" ).val(ui.value );
      }
    });

    var $table = $("#recorridosTable"),
        selections = [];

    $table.bootstrapTable({
        pageSize: 15, 
        pageList: [15],
        columns: [
            {
                field: 'id',
                title: 'Id',
                visible:false
            },{
                field: 'nombre_linea',
                title: 'Linea'
            }, {
                field: 'nombre_interno',
                title: 'Interno',
                sortable: true,
            }, {
                field: 'score',
                title: 'Puntaje',
                sortable: true,
            }, {
                field: 'iniciado',
                title: 'Iniciado',
                sortable: true,
                visible: true,
            }, {
                field: 'terminado',
                title: 'Terminado',
                sortable: true,
                visible: false,
            }],
        data: [],
    });
    $table.bootstrapTable("hideLoading");

    $("#btnViajes").click(function () {
        /*se leen los datos de la fecha y la linea para mandar a la api*/
        fecha = $("#fechaFormField").datepicker( "getDate" );
        if (fecha === null){
            alert("Debe seleccionar una fecha para poder obtener los viajes");
            return;
        }
        linea = $("#lineaSel").val();
        $("#btnViajes").button('loading');
        
        loadRecorridosFromAPI(fecha, linea)
            .done(function(data){

            $table.bootstrapTable('load', data);

            //$table.bootstrapTable('refesh');
            })
            .fail(function(err){
                console.log(err);
            })
            .always(function(){
                $("#btnViajes").button('reset');
            });

    });

    $("#btn-toogle-info").click(function (){
        toggleMapInfo();
    });

    $("#btn-toogle-legend").click(function (){
        $("#layers-box").toggle();
    });


    $("#btn-refresh-map").click(function (){

        refeshMap();
    });


    $table.on('click-row.bs.table', function (row, $element, field) {
        // click en algun viaje específico que se desea visualizar
        var viajeId = $element.id;
        var recorrido = $element.recorrido_id;
        var trayectoria = trayectoriaByRecorrido(recorrido);  // linestring geo de un recorrido en particular

        currentViaje = viajeId;
        currentRecorrido = recorrido;


        $table.find('tbody tr.active').removeClass('active table-info');
        field.addClass('active table-info');

        $umbralSlider.slider("option", "disabled", false);
        if (trayectoria === null || trayectoria === undefined){
            //TODO: hay que tomar alguna decision respecto a esto
            alert("no se pudieron obtener los datos del recorrido para el viaje actual. Revise la base de recorridos");
            return;
        }
        // dibujar el recorrido esperado en el mapa
        currentLayer = addLayerMap(trayectoria, 'recorridoLineLayer', getZIndexByLayerName('recorridoLineLayer'));
        setLayerObject('recorridoLineLayer', currentLayer);

        if ($.Toast) {
            // show toast dialog
            $.Toast.showToast({
                title: "Dibujando Mapa...",
                //duration: 1500,
                icon:"loading",
                image: ''
            });
        }

        loadDesviosFromAPI(viajeId)
        .done(function(data){
            viaje = data.viaje;
            recorrido = data.recorrido;
            recorrido = {'type':"FeatureCollection", 'features':recorrido};

            currentLayer = addLayerMap(viaje, 'viajeLayer', getZIndexByLayerName('viajeLayer'));
            setLayerObject('viajeLayer', currentLayer)

            currentLayer = addLayerMap(recorrido, 'recorridoLayer', getZIndexByLayerName('recorridoLayer'));
            setLayerObject('recorridoLayer', currentLayer)
            mapInfo = getMapInfo();
            mapInfo['media_desvios'] = data.media;
            mapInfo['indice_validacion'] = data.indice_validacion;
            mapInfo['punto_recorrido'] = undefined;

            setMapInfo(mapInfo);

            reloadMapInfo();

        })
        .fail(function (err) {
            console.log(err);
        }).always(function(){
            if ($.Toast) { $.Toast.hideToast();}
        });


    });

    function leerTrayectorias(){
        loadTrayectoria(null)
            .done(function(data){
                trayectorias = data.results.features;
            })
    }

    function trayectoriaByRecorrido(recorridoId){
        for (i = 0; i < trayectorias.length; i++){
            trayectoria = trayectorias[i];
            if (trayectoria.id === recorridoId){
                return trayectoria;
            }
        }
    }


    function loadRecorridosFromAPI(fecha, linea) {
        // boton obtener viajes para listar todos con su score
        fecha_str = fecha.toISOString().substring(0, 10);
        return $.ajax({
            method: "GET",
            url: '/data/viajes/',
            data: {
                'iniciado_date': fecha_str,
                'linea': linea
            }
        });
    }

    function loadTrackingFromAPI(recorridoId) {
        return $.ajax({
            method: "GET",
            url: '/data/viajes/' + recorridoId + '/tracking/',
        });
    }

    function loadDesviosFromAPI(recorridoId) {
        return $.ajax({
            method: "GET",
            url: '/data/viajes/' + recorridoId + '/desvios/',
        });
    }

    function loadTrayectoria(recorridoId) {
        if (recorridoId) {
            data = {'id': recorridoId};  // ex UDP-transporte 'recorrido_id': recorridoId
        } else {
            data = {};
        }
        return $.ajax({
            method: "GET",
            url: "/lineas/api/recorrido-extendido/",  // ex UDP-transporte url: '/api/v1/trayectorias/',
            data: data
        });
    }

});
