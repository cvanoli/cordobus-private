from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Max, Min
from django.utils.decorators import method_decorator
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.generic import TemplateView

from tracking.models import Tracking
from lineas.models import Linea


# class DesviosPorLineaView(LoginRequiredMixin, TemplateView):
class DesviosPorLineaView(TemplateView):
    template_name = 'desvios/desvios.html'

    @method_decorator(xframe_options_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DesviosPorLineaView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(DesviosPorLineaView, self).get_context_data(**kwargs)
        # Se le puede pedir al manager
        lineas = Linea.objects.filter(publicado=True).order_by('nombre_publico').values('id', 'nombre_publico')
        fechas = Tracking.objects.all().aggregate(Min('timestamp'), Max('timestamp'))
        context['fecha_desde'] = fechas['timestamp__min']
        context['fecha_hasta'] = fechas['timestamp__max']
        context['lineas'] = lineas
        context['titulo'] = 'Tablero de control de desvíos en el recorrido de colectivos'
        context['descripcion'] = ("Esta visualización muestra información "
            "histórica del recorrido de cada línea de colectivo en un día y "
            "hora en particular. Discrimina la geoposición del colectivo según"
            " se encuentre dentro del recorrido preestablecido o se haya "
            "desviado del mismo.</br></br>"
            "Los motivos por desvío de la unidad atienden a diferentes motivos "
            "que dependen de la decisión del chofer o de la empresa.")

        return context
