from common.helpers.funciones import desvios
from common.helpers.validacion_viajes import validar_viaje, trayecto_simplificado_celdas, obtener_grilla

def desvios_modelos(viaje, recorrido):
    '''calcula los desvios haciendo uso de las funciones abajo definidas
    pero en lugar de recibir dos arreglos recibe los modelos y se encaga de
    las transformaciones'''
    ptos_viaje = [(x['location'].y, x['location'].x) for x in viaje.values('location')]
    ptos_recorrido = [(x[1], x[0]) for x in reversed(recorrido.trazado_extendido.coords)]

    return desvios(ptos_viaje, ptos_recorrido)


def validar_viaje_modelo(viaje, recorrido):
    ptos_viaje = [(x['location'].y, x['location'].x) for x in viaje.values('location')]
    ptos_recorrido = [(x[1], x[0]) for x in reversed(recorrido.trazado_extendido.coords)]

    grilla = obtener_grilla()
    viaje_celdas = trayecto_simplificado_celdas(grilla, ptos_viaje)
    recorrido_celdas = trayecto_simplificado_celdas(grilla, ptos_recorrido)

    return validar_viaje(viaje_celdas, recorrido_celdas)
