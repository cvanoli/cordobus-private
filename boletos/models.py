from django.contrib.gis.db import models
from lineas.models import Empresa



class Chofer(models.Model):
    # Nunca se sabe si va a venir un caracter
    id_chofer_worldline = models.CharField(max_length=70, null=True, blank=True)
    # Empresa a la cual esta asociado el chofer
    empresa = models.ForeignKey(Empresa, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return 'Chofer id {} de la empresa {}'.format(self.id_chofer_worldline, self.empresa)


class ContratoUsoBoleto(models.Model):
    # usuario comun, BSC, BAM 1 (Boleto Adulto Mayor), etc
    tipo_contrato = models.CharField(max_length=70, null=True, blank=True)

    def __str__(self):
        return self.tipo_contrato


class ZonaBoleto(models.Model):
    """
    Las empresas clasifican la ciudad en diferentes zonas.
    """
    zona = models.CharField(max_length=70, null=True, blank=True)
    descripcion = models.TextField(verbose_name='Descripción de la zona')

    def __str__(self):
        return self.zona


class TarjetaRedBus(models.Model):
    numero_tarjeta = models.CharField(max_length=70, null=True, blank=True)
    ultimo_saldo = models.FloatField(blank=True, null=True)
    usos_remanentes = models.FloatField(blank=True, null=True)
    # ultima_zona = models.ForeignKey(ZonaBoleto, on_delete=models.SET_NULL, null=True, blank=True)
    ultimo_contrato = models.ForeignKey(ContratoUsoBoleto, on_delete=models.SET_NULL, null=True, blank=True)
    ultimo_lugar_de_uso = models.PointField(blank=True, null=True)

    creado = models.DateTimeField(auto_now=False, auto_now_add=True, blank=True, null=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.numero_tarjeta


# class BoletoTransporteVendido(models.Model):
#     """
#     Corte de boleto urbano
#     """
#     fecha_boleto = models.DateTimeField(null=True, blank=True)
#     hora_boleto = models.TimeField(null=True, blank=True)
#     interno = models.ForeignKey(Interno, on_delete=models.SET_NULL, null=True, blank=True)
#     linea = models.ForeignKey(Linea, on_delete=models.SET_NULL, null=True, blank=True)
#     sentido_recorrido = models.CharField(max_length=70, null=True, blank=True)
#     chofer = models.ForeignKey(Chofer, on_delete=models.SET_NULL, null=True, blank=True)
#     tarjeta = models.ForeignKey(TarjetaRedBus, on_delete=models.SET_NULL, null=True, blank=True)
#     contrato = models.ForeignKey(ContratoUsoBoleto, on_delete=models.SET_NULL, null=True, blank=True)
#     maquina = models.CharField(max_length=70, null=True, blank=True)
#     lote = models.CharField(max_length=70, null=True, blank=True)
    
#     fecha_apertura = models.DateField(null=True, blank=True)
#     hora_apertura = models.TimeField(null=True, blank=True)
#     ubicacion = models.PointField(blank=True, null=True)

#     def __str__(self):
#         return '{} - {}'.format(self.fecha_boleto, self.hora_boleto)
