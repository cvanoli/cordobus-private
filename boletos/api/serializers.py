from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework import serializers
from rest_framework_cache.registry import cache_registry
from boletos.models import ContratoUsoBoleto



class ContratoUsoBoletoSerializer(CachedSerializerMixin):

    class Meta:
        model = ContratoUsoBoleto
        fields = ['id', 'tipo_contrato']


cache_registry.register(ContratoUsoBoletoSerializer)
