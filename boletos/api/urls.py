from django.conf.urls import url, include
from rest_framework import routers
from .views import ContratoUsoBoletoViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'contrato-boletos', ContratoUsoBoletoViewSet, base_name='cortes-contrato.api.boletos')

urlpatterns = [
    url(r'^', include(router.urls)),
]
