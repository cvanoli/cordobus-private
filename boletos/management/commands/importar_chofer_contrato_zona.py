#!/usr/bin/python
from boletos.models import Chofer, ContratoUsoBoleto, ZonaBoleto
from lineas.models import Empresa
from django.core.management.base import BaseCommand
from django.db import transaction
import pandas as pd
import sys



class Command(BaseCommand):
    help = """ Comando para llenar las clases chofer, contrato y zona """

    def add_arguments(self, parser):
        parser.add_argument('--path', help='Ruta al CSV con los datos a importar.', default='/extrafs/corte_de_boleto_worldline/Pasajes_georeferenciados_20190322.csv')

    @transaction.atomic
    def handle(self, *args, **options):

        path = options['path']
        if path is None:
            self.stdout.write(self.style.ERROR('Debe especificar la ruta al archivo csv.'))
            sys.exit(1)

        self.stdout.write(self.style.SUCCESS('Importando csv desde {}'.format(path)))

        # Iniciando listas
        lista_de_chofer = []
        lista_de_contratos = []
        lista_de_zonas = []

        # DataFrame principal del csv pasado como argumento
        df = pd.read_csv(path, encoding="ISO-8859-1", sep=";", parse_dates=True)

        # Creamos otro df para manejar las columnas de empresas y choferes
        choferes = pd.DataFrame()
        choferes['EMPRESA'] = df['EMPRESA']
        choferes['CHOFER'] = df['CHOFER']
        # Eliminamos choferes duplicados
        choferes_unicos = choferes.drop_duplicates()
        # Convertimos el df a lista de listas.
        # Ej:[['Coniferal S.A.C.I.F.', 7188], ['ERSA Urbano S.A', 33014], ...]
        lista_choferes = choferes_unicos.to_numpy().tolist()

        # Creamos otro df para manejar la columna de contratos
        contrato = pd.DataFrame()
        contrato['CONTRATO'] = df['CONTRATO']
        # Eliminamos contratos duplicados
        contratos_unicos = contrato.drop_duplicates()
        # Convertimos el df a lista de listas de contratos.
        # Ej: [['Usuario común'], ['Ord. 11876 c/a'], ['BOS 2 usos'], ...]
        lista_contratos = contratos_unicos.to_numpy().tolist()

        # Creamos otro df para manejar la columna de zonas
        zona = pd.DataFrame()
        zona['ZONA'] = df['ZONA']
        # Eliminamos zonas duplicadas
        zonas_unicas = zona.drop_duplicates()
        # Convertimos el df a lista de listas de zonas
        # Ej: [[1], [6], [3], [7], [2], [8], [9], [4], [5]]
        lista_zonas = zonas_unicas.to_numpy().tolist()

        for lista in lista_choferes:
            # No me pasan id, asi que tengo que tomar los strings
            empresas = lista[0]
            id_chofer = lista[1]
            if empresas == "Coniferal S.A.C.I.F.":
                empresa = Empresa.objects.get(nombre_publico="Coniferal")
            elif empresas == "ERSA Urbano S.A":
                empresa = Empresa.objects.get(nombre_publico="ERSA")
            elif empresas == "Autobuses Córdoba S.R.L":
                empresa = Empresa.objects.get(nombre_publico="Autobuses Córdoba")
            elif empresas == "T.A.M. S.E. Trolebuses":
                empresa = Empresa.objects.get(nombre_publico="TAMSE")
            else:
                self.stdout.write(self.style.ERROR('Empresa no reconocida {}'.format(empresas)))
                sys.exit(1)

            chofer_obj, created = Chofer.objects.get_or_create(id_chofer_worldline=id_chofer, empresa=empresa)
            if created:
                lista_de_chofer.append(chofer_obj.id_chofer_worldline)

        for contrato in lista_contratos:
            contrato_str = contrato[0]
            contrato_obj, created = ContratoUsoBoleto.objects.get_or_create(tipo_contrato=contrato_str)
            if created:
                lista_de_contratos.append(contrato_obj.tipo_contrato)

        for zona in lista_zonas:
            zona_str = str(zona[0])
            zona_obj, created = ZonaBoleto.objects.get_or_create(zona=zona_str)
            if created:
                lista_de_zonas.append(zona_obj.zona)


        self.stdout.write(self.style.SUCCESS('Choferes nuevos grabados {}: {}'.format(len(lista_de_chofer), lista_de_chofer)))
        self.stdout.write(self.style.SUCCESS('Contratos nuevos grabados {}: {}'.format(len(lista_de_contratos), lista_de_contratos)))
        self.stdout.write(self.style.SUCCESS('Zonas nuevas grabadas {}: {}'.format(len(lista_de_zonas), lista_de_zonas)))
        self.stdout.write(self.style.SUCCESS('Fin del script.'))
