from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from tableros.views import index


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    path('tableros/', include('tableros.urls')),
    path('data/', include('tracking.urls')),
    path('lineas/api/', include('lineas.api.urls')),
    path('analisis/api/', include('analisis_boletos.api.urls')),
    path('boletos/api/', include('boletos.api.urls')),
    path('paradas/', include('paradas.urls')),
    path('analisis/', include('analisis_boletos.urls')),
    path('mapas/', include('mapas.urls')),
    path('colectivos/', include('colectivos.urls')),

    # API's
    path('paradas-colectivos/api/', include('paradas.api.urls')),
    path('tracking/api/', include('tracking.api.urls')),

    #GTFS
    path('gtfs/', include('lineas.gtfs.urls')),
]
