import os
from kombu import Exchange, Queue
from celery.schedules import crontab


# Añadimos la siguiente opción url del broker al que se conectará celery.
CELERY_BROKER_URL='redis://localhost:6379'
CELERY_RESULT_BACKEND='redis://localhost:6379'

#Redis

BROKER_URL = ''

# We don't want to have dead connections stored on rabbitmq, so we have to negotiate using heartbeats
BROKER_HEARTBEAT = '?heartbeat=30'
if not BROKER_URL.endswith(BROKER_HEARTBEAT):
    BROKER_URL += BROKER_HEARTBEAT

BROKER_POOL_LIMIT = 1
BROKER_CONNECTION_TIMEOUT = 10

# Celery configuration

# configure queues, currently we have only one
CELERY_DEFAULT_QUEUE = 'default'
CELERY_QUEUES = (
    Queue('default', Exchange('default'), routing_key='default'),
)

# los mjs de tareas son reconocidos después de que se haya ejecutado la tarea
CELERY_ACKS_LATE = True

# si se almacena o no los valores de retorno de la tarea
CELERY_IGNORE_RESULT = True

CELERY_REDIS_MAX_CONNECTIONS = 1

# Don't use pickle as serializer, json is much safer
CELERY_TASK_SERIALIZER = "json"
CELERY_ACCEPT_CONTENT = ['application/json']

# Se puede personalizar los propios manejadores de registro(logging)
CELERYD_HIJACK_ROOT_LOGGER = False

# http://celery.readthedocs.io/en/latest/userguide/configuration.html#std:setting-worker_prefetch_multiplier
CELERYD_PREFETCH_MULTIPLIER = 1

# maximo numero de tareas por proceso
CELERYD_MAX_TASKS_PER_CHILD = 1000


# configurar las tareas periodicas acá(para celery beat)
CELERY_BEAT_SCHEDULE = {
    'importar_cortes_boleto': {
        'task': 'analisis_boletos.tasks.importar_cortes_boleto',
        'schedule': crontab(hour='9', minute='31'),
    },
    'importar_cortes_boleto_1': {
        'task': 'analisis_boletos.tasks.importar_cortes_boleto',
        'schedule': crontab(hour='10', minute='02'),
    },
    # por si todavía no fue subido el archivo,
    # se intenta nuevamente a las 5 de la tarde
    'importar_cortes_boleto_intento_2': {
        'task': 'analisis_boletos.tasks.importar_cortes_boleto',
        'schedule': crontab(hour='16', minute='57'),
    },
    #se ejecuta cada 5 minutos
    'grabar_frecuencias': {
        'task': 'tracking.tasks.grabar_frecuencias',
        'schedule': crontab(hour='*', minute='*/5'),
    }
}

CELERY_TIMEZONE = 'America/Argentina/Cordoba'

try:
    from .local_settings import *
except ImportError:
    pass