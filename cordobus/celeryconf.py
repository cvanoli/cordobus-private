from __future__ import absolute_import, unicode_literals
import os
from celery import Celery


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cordobus.settings')

app = Celery('cordobus')

# Usando un string acá significa que el worker no tiene que serializar
# el objeto de configuración para procesos secundarios
# - namespace='CELERY' significa que todas las claves de configuración de celery
#   deben tener `CELERY_` como prefijo.
app.config_from_object('cordobus.settings_celery', namespace='CELERY')

# Este método auto-registra las tareas para el broker.
# Busca tareas dentro de todos los archivos `tasks.py` que haya en las apps
# y las envía a Redis automáticamente.
app.autodiscover_tasks()