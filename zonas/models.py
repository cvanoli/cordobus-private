from django.contrib.gis.db import models


class Zonas(models.Model):
    ''' zonas con algun interés '''
    nombre = models.CharField(max_length=90)
    es_zona_de_fuera_de_servicio = models.BooleanField(default=False)
    es_media_punta = models.BooleanField(default=False)
    poligono = models.PolygonField(null=True, blank=True)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre
