from django.shortcuts import render
from django.views.generic import TemplateView
from lineas.models import Recorrido


class MapaUnidadesActivasView(TemplateView):
    template_name = "mapa_unidades_activas.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(MapaUnidadesActivasView, self).get_context_data(**kwargs)
        context['recorridos'] = Recorrido.objects.filter(publicado=True, trazado__isnull=False).order_by('linea__nombre_publico', 'descripcion_corta')
        context['descripcion'] = ('En esta sección se puede ver dónde están '
            'cada una de las unidades del transporte urbano de '
            'pasajeros en la ciudad de Córdoba. Se utiliza el GPS de cada '
            'unidad de transporte. Cada ubicación muestra los '
            'siguientes datos: el número de interno, número de línea, empresa, '
            'recorrido, velocidad promedio y momento del último dato enviado '
            'por la unidad. <br/><br/>'
            'El chofer tiene la responsabilidad de loguarse cuando inicia un '
            'recorrido, avisando qué recorrido va a realizar. En caso de no '
            'loguearse el sistema detecta una unidad en la calle pero no la '
            'identifica bajo una línea y recorrido puntual. A este último caso '
            'lo localizamos bajo la leyenda “Desconocido”. <br/><br/>'
            'Estos datos son la base de la funcionalidad de la <a href='
            '"https://play.google.com/store/apps/details?id=ar.gob.cordoba.'
            'gobiernoabierto.go&hl=es_419">app GO<a/>.')
        return context

class MapaUnidadesActivasMobileView(TemplateView):
    template_name = "mapa_unidades_activas_mobile.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(MapaUnidadesActivasMobileView, self).get_context_data(**kwargs)
        return context
