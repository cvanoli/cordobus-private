from django.urls import path
from django.conf.urls import url
from django.conf.urls import include
from mapas.views import MapaUnidadesActivasView, MapaUnidadesActivasMobileView


urlpatterns = [
    path('mapa-unidades-activas', MapaUnidadesActivasView.as_view(), name='mapas.unidades-activas'),
    path('mapa-unidades-activas-mobile', MapaUnidadesActivasMobileView.as_view(), name='mapas.unidades-activas-mobile'),
]